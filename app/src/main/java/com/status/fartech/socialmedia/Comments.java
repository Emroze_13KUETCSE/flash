package com.status.fartech.socialmedia;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class Comments extends AppCompatActivity {

    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    ArrayList<listitems_comments> listelements;
    ImageView back,imageView77;
    ListView mainlist;
    TextView noevents;
    SwipeRefreshLayout mSwipeRefreshLayout;
    JSONObject jsonObject;
    JSONArray jsonArray;
    JSONObject JO;
    String JSON_STRING, username, uid, comment, time, postid, ppurl;
    ListAdapter adapter;
    CircleImageView commenter;
    EditText writecomment;
    TextView post;

    String ppurll,usernamee,uidd,postttid;
    private FirebaseAuth mAuth;
    String urll="http://heyapp.in/mediasendcomments.php";

    private DisplayImageOptions options,options2;

    String useridd;

    ProgressBar progressBar;


    int commentCounter=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();
        useridd = preference.getString("uid","");

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_profile2)
                .showImageForEmptyUri(R.mipmap.ic_profile2)
                .showImageOnFail(R.mipmap.ic_profile2)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 1))
                .build();

        Intent intent=getIntent();

        postttid=intent.getStringExtra("postid");
        listelements=new ArrayList<listitems_comments>();
        back=(ImageView)findViewById(R.id.imv_back);
        mainlist=(ListView)findViewById(R.id.list_comments);
        commenter=(CircleImageView)findViewById(R.id.circlecommenter);
        writecomment=(EditText)findViewById(R.id.et_entercomment);
        post=(TextView)findViewById(R.id.tv_posst);
        progressBar = findViewById(R.id.progress);
        mAuth = FirebaseAuth.getInstance();


        ppurll=preference.getString("ppurl","");
        usernamee=preference.getString("name","");
        uidd=preference.getString("uid","");


        if (!ppurll.equals("")){
            Picasso.get().load(ppurll).into(commenter);

        }else {
            commenter.setImageResource(R.mipmap.ic_profile2);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Comments.super.onBackPressed();
            }
        });


        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String commen=writecomment.getText().toString();

                if(!commen.isEmpty()){
                    post.setEnabled(false);
                    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMddHHmm");
                    final String currenttime=simpleDateFormat.format(new Date());
                    writecomment.getText().clear();
                    StringRequest stringrequest = new StringRequest(Request.Method.POST, urll,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                    listelements.clear();
                                    new BackgroundTask().execute();

                                    post.setEnabled(true);
                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            if (!usernamee.equals("")){
                                params.put("username", usernamee);
                            }else {
                                params.put("username", "Unknown");
                            }
                            if (!ppurll.equals("")){
                                params.put("photourl", ppurll);
                            }else {
                                params.put("photourl", "Not Found");
                            }
                            if (!uidd.equals("")){
                                params.put("Uid",uidd);
                            }else {
                                params.put("Uid","Not Found");
                            }
                            params.put("comment",commen);
                            params.put("time",currenttime);
                            params.put("postid",postttid);

                            return params;
                        }
                    };
                    RequestQueue requestQueue= Volley.newRequestQueue(Comments.this);
                    requestQueue.add(stringrequest);
                }

            }
        });

        noevents = (TextView) findViewById(R.id.textView50);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (listelements != null) {
                    listelements.clear();
                    new BackgroundTask().execute();
                }
            }
        });



        new BackgroundTask().execute();

    }

    class BackgroundTask extends AsyncTask<Void, Void, String> {

        String json_url;

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            json_url = "http://heyapp.in/showcommentsmedia.php";
            commentCounter=0;
        }


        @Override
        protected String doInBackground(Void... params) {

            try {



                URL url = new URL(json_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();

                while ((JSON_STRING = bufferedReader.readLine()) != null) {
                    stringBuilder.append(JSON_STRING).append("\n");
                }
                jsonObject = new JSONObject(String.valueOf(stringBuilder));
                jsonArray = jsonObject.getJSONArray("eventss");


                int count = 0;

                while (count < jsonArray.length()) {


                    JO = jsonArray.getJSONObject(count);

                    username = JO.getString("username");
                    uid = JO.getString("uid");
                    time = JO.getString("time");
                    postid = JO.getString("postid");
                    comment = JO.getString("comment");
                    ppurl = JO.getString("ppurl");


                    if (postttid.equals(postid)){
                        if (!ppurl.equals("Not Found")){

                            listelements.add(new listitems_comments(null,username,comment,time,uid,postid,ppurl));

                            commentCounter++;

                            adapter = new ListAdapter(listelements);
                        }







                    }


                    count++;
                }



                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();

                return stringBuilder.toString().trim();

            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate();
        }


        @Override
        public void onPostExecute(String result) {

            if (listelements.isEmpty()) {
                noevents.setVisibility(View.VISIBLE);
            } else {
                noevents.setVisibility(View.INVISIBLE);
            }

            mainlist.setAdapter(adapter);
            mSwipeRefreshLayout.setRefreshing(false);
            progressBar.setVisibility(View.GONE);

        }


    }

    static class ViewHolder {
        CircleImageView pp;
        TextView usernm, comm, timm;
    }

    class ListAdapter extends BaseAdapter {

        ArrayList<listitems_comments> Listitems = new ArrayList<listitems_comments>();

        ListAdapter(ArrayList<listitems_comments> listite) {
            this.Listitems = listite;
        }

        @Override
        public int getCount() {
            return Listitems.size();

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return Listitems.get(position).postid;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;
            LayoutInflater layoutInflater = getLayoutInflater();

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.commentsitem, null);
                viewHolder = new ViewHolder();
                viewHolder.pp = (CircleImageView) convertView.findViewById(R.id.circlecomment);

                viewHolder.usernm = (TextView) convertView.findViewById(R.id.tv_usernameee);
                viewHolder.comm = (TextView) convertView.findViewById(R.id.tv_commenttt);
                viewHolder.timm = (TextView) convertView.findViewById(R.id.tv_commenttime);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMddHHmm");
            final String currenttime=simpleDateFormat.format(new Date());

            viewHolder.pp.setImageBitmap(Listitems.get(position).userphoto);
            viewHolder.usernm.setText(String.valueOf(Listitems.get(position).username));
            viewHolder.comm.setText(String.valueOf(Listitems.get(position).comment));

            viewHolder.pp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(useridd.equals(Listitems.get(position).uid)){
                        Intent intent = new Intent(getApplicationContext(),Profile.class);
                        startActivity(intent);

                    }else {
                        Intent intent = new Intent(getApplicationContext(),OtherUserProfile.class);
                        intent.putExtra("uid",Listitems.get(position).uid);
                        intent.putExtra("userName",Listitems.get(position).username);
                        intent.putExtra("imgUrl",Listitems.get(position).ppurl);
                        startActivity(intent);
                    }
                }
            });

            viewHolder.usernm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(useridd.equals(Listitems.get(position).uid)){
                        Intent intent = new Intent(getApplicationContext(),Profile.class);
                        startActivity(intent);

                    }else {
                        Intent intent = new Intent(getApplicationContext(),OtherUserProfile.class);
                        intent.putExtra("uid",Listitems.get(position).uid);
                        intent.putExtra("userName",Listitems.get(position).username);
                        intent.putExtra("imgUrl",Listitems.get(position).ppurl);
                        startActivity(intent);
                    }
                }
            });

            ImageLoader.getInstance()
                    .displayImage(Listitems.get(position).ppurl,viewHolder.pp, options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            //Toast.makeText(context, "Complete", Toast.LENGTH_SHORT).show();
                            viewHolder.pp.setImageBitmap(loadedImage);

                            //carsViewHolder.mExoPlayer.getLayoutParams().height = carsViewHolder.imgCover.getHeight();
                            //carsViewHolder.mExoPlayer.requestFocus();

                        }
                    }, new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                            //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                        }
                    });


            Date commentdate=null;
            Date current = null;
            try {
                commentdate=simpleDateFormat.parse(Listitems.get(position).time);
                current=simpleDateFormat.parse(currenttime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long timediff=current.getTime()-commentdate.getTime();
            long timediff2=timediff/1000;
            long timediffmin=timediff2/60;
            long timediffhour=timediffmin/60;

            if (timediffmin<60&&timediffmin>0){
                viewHolder.timm.setText(String.valueOf(timediffmin)+" m");
            }else if (timediffmin>60){
                viewHolder.timm.setText(String.valueOf(timediffhour)+" h");
            }else {
                viewHolder.timm.setText("now");
            }









            return convertView;
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("commentCounter",commentCounter);
        setResult(11 , intent);
        System.out.println("only ff result back "+commentCounter);
        super.onBackPressed();
    }

    private void updateUI2(FirebaseUser currentUser) {

    }
}
