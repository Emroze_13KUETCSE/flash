package com.status.fartech.socialmedia;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class LogInBottomSheet extends BottomSheetDialogFragment {
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    String pppurl="";

    Context context;


    ImageView imageClose;


    View rootView;

    private int lastVisibleItem = 0;
    private int firstY = 0;

    BottomSheetBehavior bottomSheetBehavior;


    EditText etName,etNumber,etCountryCode,etOtp;
    TextView tvNext,tvVerify;

    RelativeLayout rlRoot,rlVerify,rlInfo;
    LinearLayout ll;

    boolean isOpen=false;
    boolean isOpen2=false;


    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;

    String mVerificationId;
    PhoneAuthProvider.ForceResendingToken mResendToken;

    CircleImageView proPic;

    private static final int SELECT_PHOTO = 100;
    Uri selectedImageURI = null;
    public Bitmap yourSelectedImage;
    EditText etName2;
    String gender="";
    RadioButton radioMale,radioFemale;
    RadioGroup radioGroupGender;


    byte[] byteArrayImage;

    String urll="http://heyapp.in/mediausers.php";

    String accountId = "0023d17fbbe68840000000001"; // Obtained from your B2 account page.
    String applicationKey = "K002OQPFP320nWvEwH+hRuSZerk2jBI"; // Obtained from your B2 account page.
    HttpURLConnection connectionn = null;
    String headerForAuthorizeAccount = "Basic " + Base64.encodeToString((accountId + ":" + applicationKey).getBytes(),Base64.DEFAULT);

    String apiUrl = ""; // Provided by b2_authorize_account
    String accountAuthorizationToken = ""; // Provided by b2_authorize_account
    String bucketId = ""; // The ID of the bucket you want to upload your file to
    String postParams="";
    byte postData[];
    HttpURLConnection connectionuploadurl = null;
    String currenttime;
    String uploadUrl = ""; // Provided by b2_get_upload_url
    String uploadAuthorizationToken = ""; // Provided by b2_get_upload_url
    String fileName = ""; // The name of the file you are uploading
    String contentType = ""; // The content type of the file
    String useridd = ""; // SHA1 of the file you are uploading
    byte[] fileData;
    HttpURLConnection connectionupload = null;
    String json = null;
    String jsonResponse,fileid,gend;

    ProgressBar progressBar;

    private static final String Send_Data_URL = "http://heyapp.in/sendpp.php";


    TextView tvComplete;


    LinearLayout llGoogle,llfb;
    GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN=101;

    String signInMethod="";

    CallbackManager callbackManager;

    LoginButton loginButton;
    private static final String EMAIL = "email";


    TextView tv1,tv2;
    LinearLayout rl_terms;

    public static LogInBottomSheet getInstance() {
        return new LogInBottomSheet();
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                if(firstY == 0){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_DRAGGING);
                }else if(firstY == 1){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }

            }

            System.out.println("djhfgjhsgfjhgsdjh");
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.login, null);
        dialog.setContentView(contentView);

        rootView = contentView;

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent())
                .getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));


        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        View parent = (View) contentView.getParent();
        parent.setFitsSystemWindows(true);
        bottomSheetBehavior = BottomSheetBehavior.from(parent);
        contentView.measure(0, 0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        bottomSheetBehavior.setPeekHeight(screenHeight);

        if (params.getBehavior() instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior)params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        params.height = screenHeight-convertDpIntoPx(getContext(),0);

        parent.setLayoutParams(params);


        tv1 = contentView.findViewById(R.id.tv1);
        tv2 = contentView.findViewById(R.id.tv2);

        imageClose = contentView.findViewById(R.id.img_close);

        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isOpen){
                    viewMenu();
                }
                else {
                    dialog.dismiss();
                }

            }
        });

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.service_agg)));
                startActivity(browserIntent);
            }
        });

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacy)));
                startActivity(browserIntent);
            }
        });


        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);

        preference = getContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();


        rlRoot = contentView.findViewById(R.id.rl_root);
        rlVerify = contentView.findViewById(R.id.rl_verify);
        rlInfo = contentView.findViewById(R.id.rl_userInfo);
        rl_terms = contentView.findViewById(R.id.rl_terms);

        ll = contentView.findViewById(R.id.ll_log_in);
        tvNext = contentView.findViewById(R.id.tv_next);
        etName = contentView.findViewById(R.id.et_name);
        etNumber = contentView.findViewById(R.id.et_number);
        etCountryCode = contentView.findViewById(R.id.et_code);
        etOtp = contentView.findViewById(R.id.et_otp);
        tvVerify = contentView.findViewById(R.id.tv_verify);
        proPic = contentView.findViewById(R.id.img_pro_pic);
        etName2 = contentView.findViewById(R.id.et_name2);
        progressBar = contentView.findViewById(R.id.progress_bar);
        radioGroupGender = contentView.findViewById(R.id.radioGroupGender);
        tvComplete = contentView.findViewById(R.id.tv_complete);
        radioMale = contentView.findViewById(R.id.radioMale);
        radioFemale = contentView.findViewById(R.id.radioFemale);
        llGoogle = contentView.findViewById(R.id.ll_google);
        llfb = contentView.findViewById(R.id.ll_facebook);

        loginButton = contentView.findViewById(R.id.login_button);
        mAuth = FirebaseAuth.getInstance();

        etNumber.requestFocus();



        gender="male";
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(checkedId == R.id.radioMale){
                    gender = "male";
                }
                else if(checkedId == R.id.radioFemale){
                    gender = "female";
                }
            }
        });

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //String name = etName.getText().toString();
                String code = etCountryCode.getText().toString();
                String number = etNumber.getText().toString();

                if(code.isEmpty() || number.isEmpty()){

                }else {
                    isOpen=false;
                    viewMenu();

                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            code+number,        // Phone number to verify
                            60,                 // Timeout duration
                            TimeUnit.SECONDS,   // Unit of timeout
                            getActivity(),               // Activity (for callback binding)
                            mCallbacks);        // OnVerificationStateChangedCallbacks
                }
            }
        });


        tvVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String otp = etOtp.getText().toString();

                if(otp.isEmpty()){
                    Toast.makeText(getContext(), "Enter OTP", Toast.LENGTH_SHORT).show();
                }else {
                    signInMethod="phone";
                    progressBar.setVisibility(View.VISIBLE);
                    PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential(mVerificationId,otp);
                    signInWithPhoneAuthCredential(phoneAuthCredential);
                }
            }
        });

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);

        llGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                signInMethod = "google";
                progressBar.setVisibility(View.VISIBLE);
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        //callbackManager = CallbackManager.Factory.create();
        //loginButton.setReadPermissions(Arrays.asList(EMAIL));

        /*loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                handleFacebookAccessToken(loginResult.getAccessToken());
                System.out.println("only pp =======");
            }

            @Override
            public void onCancel() {
                // App code
                System.out.println("only pp ok");
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("only pp ok"+exception.toString());
                // App code
            }
        });*/

        callbackManager = CallbackManager.Factory.create();

        llfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInMethod = "facebook";

                System.out.println("pp      ?????????????  start ");
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("email","public_profile"));

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                // App code
                                System.out.println("pp      ?????????????  ok");
                                handleFacebookAccessToken(loginResult.getAccessToken());
                            }

                            @Override
                            public void onCancel() {
                                // App code
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                // App code
                                System.out.println("pp      ?????????????  "+ exception.toString());
                            }
                        });

                System.out.println("only      ?????????????  click");

            }
        });


        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.

                String code = credential.getSmsCode();

                etOtp.setText(code);

                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.


                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request

                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded

                }

            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {

                mVerificationId = verificationId;
                mResendToken = token;

            }
        };


        proPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });


        tvComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(signInMethod.equals("google")|| signInMethod.equals("facebook")){
                    progressBar.setVisibility(View.VISIBLE);

                    try{
                        Bitmap decoded = ((BitmapDrawable)proPic.getDrawable()).getBitmap();;
                        if(decoded == null){
                            Toast.makeText(getContext(), "Please select Image", Toast.LENGTH_SHORT).show();
                        }else {

                            String name = etName2.getText().toString();

                            if(!name.isEmpty() ){
                                System.out.println();
                                if(true){
                                    //Toast.makeText(getContext(), "Ok check-box", Toast.LENGTH_SHORT).show();

                                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                                    decoded.compress(Bitmap.CompressFormat.JPEG, 80, out);
                                    yourSelectedImage = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

                                    byteArrayImage = out.toByteArray();

                                    fileData = byteArrayImage;

                                    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
                                    String currenttime=simpleDateFormat.format(new Date());

                                    fileName=useridd+currenttime+".jpg";
                                    contentType="image/jpeg";

                                    pppurl="https://static.heyapp.in/file/app-media/"+useridd+currenttime+".jpg";

                                    new connecttt().execute();

                                }
                                else {
                                    Toast.makeText(getContext(), "Select check-box", Toast.LENGTH_SHORT).show();
                                }

                            }
                            else {
                                Toast.makeText(getContext(), "Enter your name", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }catch (NullPointerException eee){
                        Toast.makeText(getContext(), "Please select Image", Toast.LENGTH_SHORT).show();
                    }



                }else {
                    if(yourSelectedImage == null){
                        Toast.makeText(getContext(), "Please select Image", Toast.LENGTH_SHORT).show();
                    }else {
                        progressBar.setVisibility(View.VISIBLE);

                        String name = etName2.getText().toString();

                        if(!name.isEmpty() ){

                            if(true){

                                new connecttt().execute();

                            }
                            else {
                                Toast.makeText(getContext(), "Select check-box", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else {
                            Toast.makeText(getContext(), "Enter your name", Toast.LENGTH_SHORT).show();
                        }


                    }
                }

            }
        });

    }

    public void getUser(final String uidd){

        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://heyapp.in/showusers.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObjecct = new JSONObject(response);
                            JSONArray jsonArraay = jsonObjecct.getJSONArray("eventss");

                            int j = 0;
                            String ppurll = "";
                            String userrname = "";
                            String uiddi="";
                            for(int i = 0 ; i < jsonArraay.length(); i++){
                                JSONObject obj = jsonArraay.getJSONObject(i);
                                String uid = obj.getString("uid");

                                if (uid.equals(uidd)){
                                    
                                    userrname = obj.getString("Username");
                                    uiddi = obj.getString("uid");
                                    ppurll = obj.getString("photourl");
                                    j=1;
                                    System.out.println("only >> uid3 google " + uidd);

                                    break;
                                }

                                
                            }
                            
                            if(j == 0){
                                etName2.setText("");

                                hideKeyboard(getActivity());
                                viewMenuInfo();
                            }else {
                                etName2.setText(userrname);

                                hideKeyboard(getActivity());


                                if(ppurll.isEmpty()){
                                    getUser2(uidd,userrname);
                                }else {
                                    progressBar.setVisibility(View.GONE);
                                    editor.putString("uid",uidd);
                                    editor.putString("name",userrname);
                                    editor.putString("ppurl",ppurll);
                                    editor.commit();

                                    dismiss();

                                    Intent intent = new Intent(getContext(),Profile.class);
                                    startActivity(intent);

                                    System.out.println("only >> uid4 google " + uidd);


                                }



                                //viewMenuInfo();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Uid",useridd);
                params.put("Following", itemArrayList2.get(position).getUid());


                return params;
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void getUser2(final String uidd,final String uName){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://heyapp.in/showposts.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObjecct = new JSONObject(response);
                            JSONArray jsonArraay = jsonObjecct.getJSONArray("eventss");

                            int j = 0;
                            String ppurll = "";
                            String userrname = "";
                            String uiddi="";
                            for(int i = 0 ; i < jsonArraay.length(); i++){
                                JSONObject obj = jsonArraay.getJSONObject(i);
                                String uid = obj.getString("uid");

                                if (uid.equals(uidd)){
                                    System.out.println("only >> uid5 google " + uidd);
                                    userrname = obj.getString("username");
                                    uiddi = obj.getString("uid");
                                    ppurll = obj.getString("ppurl");
                                    j=1;
                                    break;
                                }


                            }

                            if(j == 0){
                                etName2.setText("");
                                viewMenuInfo();
                            }else {
                                //etName2.setText(userrname);

                                hideKeyboard(getActivity());

                                progressBar.setVisibility(View.GONE);
                                editor.putString("uid",uidd);
                                editor.putString("name",userrname);
                                editor.putString("ppurl",ppurll);
                                editor.commit();

                                dismiss();

                                Intent intent = new Intent(getContext(),Profile.class);
                                startActivity(intent);

                                System.out.println("only >> uid6 google " + uidd);

                                //viewMenuInfo();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Uid",useridd);
                params.put("Following", itemArrayList2.get(position).getUid());


                return params;
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    private void handleFacebookAccessToken(AccessToken accessToken) {

        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser user = mAuth.getCurrentUser();

                            String name = user.getDisplayName();
                            String email = user.getEmail();
                            Uri photoUrl = user.getPhotoUrl();


                            String uid=user.getUid();

                            System.out.println("only + >>>>>>>>>>>" + uid);

                            useridd = uid;

                            editor.putString("firebase_uid",uid);
                            editor.commit();

                            etName2.setText(name);

                            Picasso.get().load(photoUrl).into(proPic);

                            hideKeyboard(getActivity());
                            viewMenuInfo();

                        } else {
                            System.out.println("only 777777777  ");
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }


                    }
                });
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser user = task.getResult().getUser();
                            String uid=user.getUid();

                            System.out.println("only >>>>>>>>>>> uid1" + uid);

                            useridd = uid;

                            getUser(useridd);

                        } else {
                            // Sign in failed, display a message and update the UI

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("only      ?????????????  result");
        /*try{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }catch (NullPointerException w){
            System.out.println("hhhh  >>>>>>>>>>>>>>>>>>>>  only      ?????????????  result");
        }*/


        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK){
                    selectedImageURI = data.getData();
                    InputStream imageStream = null;
                    try {
                        imageStream = getContext().getContentResolver().openInputStream(selectedImageURI);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Bitmap decoded = BitmapFactory.decodeStream(imageStream);

                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
                    yourSelectedImage = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

                    proPic.setImageBitmap(yourSelectedImage);

                    byteArrayImage = out.toByteArray();

                    fileData = byteArrayImage;

                    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
                    String currenttime=simpleDateFormat.format(new Date());

                    fileName=useridd+currenttime+".jpg";
                    contentType="image/jpeg";

                    pppurl="https://static.heyapp.in/file/app-media/"+useridd+currenttime+".jpg";

                    //String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);


                }
        }

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately

            }
        }



    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            FirebaseUser user = mAuth.getCurrentUser();

                            String name = user.getDisplayName();
                            String email = user.getEmail();
                            Uri photoUrl = user.getPhotoUrl();


                            String uid=user.getUid();

                            System.out.println("only >> uid2 google " + uid);

                            useridd = uid;

                            etName2.setText(name);

                            getUser(useridd);

                        } else {
                            // If sign in fails, display a message to the user.

                        }

                    }
                });
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void viewMenu() {

        if (!isOpen) {

            int x = (int) (rlRoot.getRight()/2);
            int y = (int) rlRoot.getBottom();

            int startRadius = 0;
            int endRadius = (int) Math.hypot(rlRoot.getWidth(), rlRoot.getHeight());

            //fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(),android.R.color.white,null)));
            //fab.setImageResource(R.drawable.ic_close_grey);

            Animator anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(rlVerify, x, y, startRadius, endRadius);
                rlVerify.setVisibility(View.VISIBLE);

                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        ll.setVisibility(View.GONE);


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

                anim.start();
            }
            else {
                rlVerify.setVisibility(View.VISIBLE);
                ll.setVisibility(View.GONE);

            }


            isOpen = true;

        } else {

            int x = (int) (rlRoot.getRight()/2);
            int y = (int) rlRoot.getBottom()-100;

            int startRadius = Math.max(rlRoot.getWidth(), rlRoot.getHeight());
            int endRadius = 0;

            //fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(),R.color.colorAccent,null)));
            //fab.setImageResource(R.drawable.ic_plus_white);
            ll.setVisibility(View.VISIBLE);
            Animator anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(rlVerify, x, y, startRadius, endRadius);
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        rlVerify.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
                anim.start();
            }
            else {
                rlVerify.setVisibility(View.GONE);

            }

            isOpen = false;
        }
    }

    private void viewMenuInfo() {
        progressBar.setVisibility(View.GONE);
        if (!isOpen2) {

            int x = (int) (rlRoot.getRight()/2);
            int y = (int) rlRoot.getBottom();

            int startRadius = 0;
            int endRadius = (int) Math.hypot(rlRoot.getWidth(), rlRoot.getHeight());

            //fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(),android.R.color.white,null)));
            //fab.setImageResource(R.drawable.ic_close_grey);

            Animator anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(rlInfo, x, y, startRadius, endRadius);
                rlInfo.setVisibility(View.VISIBLE);
                rl_terms.setVisibility(View.VISIBLE);

                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        ll.setVisibility(View.GONE);


                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

                anim.start();
            }
            else {
                rlInfo.setVisibility(View.VISIBLE);
                rl_terms.setVisibility(View.VISIBLE);
                rlVerify.setVisibility(View.GONE);

            }


            isOpen2 = true;

        } else {

            int x = (int) (rlRoot.getRight()/2);
            int y = (int) rlRoot.getBottom()-100;

            int startRadius = Math.max(rlRoot.getWidth(), rlRoot.getHeight());
            int endRadius = 0;

            //fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(),R.color.colorAccent,null)));
            //fab.setImageResource(R.drawable.ic_plus_white);
            rlVerify.setVisibility(View.VISIBLE);
            Animator anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(rlInfo, x, y, startRadius, endRadius);
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        rlInfo.setVisibility(View.GONE);
                        rl_terms.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
                anim.start();
            }
            else {
                rlInfo.setVisibility(View.GONE);
                rl_terms.setVisibility(View.GONE);
            }

            isOpen2 = false;
        }
    }

    public static int convertDpIntoPx(Context mContext, float yourdpmeasure) {
        if (mContext == null) {
            return 0;
        }
        Resources r = mContext.getResources();
        int px = (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, yourdpmeasure, r.getDisplayMetrics());
        return px;
    }


    class connecttt extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new connecttt2().execute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL urll = new URL("https://api.backblazeb2.com/b2api/v2/b2_authorize_account");
                connectionn = (HttpURLConnection)urll.openConnection();
                connectionn.setRequestMethod("GET");
                connectionn.setRequestProperty("Authorization", headerForAuthorizeAccount);
                InputStream in = new BufferedInputStream(connectionn.getInputStream());
                jsonResponse = myInputStreamReader(in);
                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("bucketId")){
                        String[] splitter=s.split("\"");
                        bucketId=splitter[3];
                        postParams = "{\"bucketId\":\"" + bucketId + "\"}";
                        postData= postParams.getBytes(StandardCharsets.UTF_8);


                    }else if (s.contains("apiUrl")){
                        String[] splitter=s.split("\"");
                        apiUrl=splitter[3];
                    }else if (s.contains("authorizationToken")){
                        String[] splitter=s.split("\"");
                        accountAuthorizationToken=splitter[3];
                    }
                }





            } catch (Exception e) {
                e.printStackTrace();


            } finally {
                connectionn.disconnect();
            }

            return null;
        }
    }


    class connecttt2 extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            new connecttt3().execute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL url = new URL(apiUrl + "/b2api/v2/b2_get_upload_url");
                connectionuploadurl = (HttpURLConnection)url.openConnection();
                connectionuploadurl.setRequestMethod("POST");
                connectionuploadurl.setRequestProperty("Authorization", accountAuthorizationToken);
                connectionuploadurl.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connectionuploadurl.setRequestProperty("charset", "utf-8");
                connectionuploadurl.setRequestProperty("Content-Length", Integer.toString(postData.length));
                connectionuploadurl.setDoOutput(true);
                DataOutputStream writer = new DataOutputStream(connectionuploadurl.getOutputStream());
                writer.write(postData);
                String jsonResponse = myInputStreamReader(connectionuploadurl.getInputStream());


                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("uploadUrl")){
                        String[] splitter=s.split("\"");
                        uploadUrl=splitter[3];


                    }else if (s.contains("authorizationToken")){
                        String[] splitter=s.split("\"");
                        uploadAuthorizationToken=splitter[3];
                    }
                }



            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connectionuploadurl.disconnect();
            }

            return null;
        }
    }

    class connecttt3 extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //uploadpp send_data=new uploadpp( username.getText().toString(),useridd,gend,fileid,responseListener);
            final String username = etName2.getText().toString();

            System.out.println("rrrrrrrrrrrrrrrrrrr**************");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Send_Data_URL,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {
                            // response

                            System.out.println("only rrrrrrrrrrrrrrrrrrr**************    okkkkkk "+response);

                            System.out.println("only url "+pppurl);

                            progressBar.setVisibility(View.GONE);


                            editor.putString("uid",useridd);
                            editor.putString("name",username);
                            editor.putString("ppurl",pppurl);
                            editor.putString("gender",gender);
                            editor.commit();

                            dismiss();

                            Intent intent = new Intent(getContext(),Profile.class);
                            startActivity(intent);
                            //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            System.out.println("only rrrrrrrrrrrrrrrrrrr**************"+error.toString());
                        }
                    }
            ) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", ACCESS_TOKEN);
                return params;
            }*/

                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("username",username);
                    params.put("uid",useridd);
                    params.put("gender",gender);
                    params.put("ppurl",pppurl);

                    return params;
                }
                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    //statusCode = response.statusCode;
                    return super.parseNetworkResponse(response);
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    1*2000,
                    2,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);



        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL url = new URL(uploadUrl);
                connectionupload = (HttpURLConnection)url.openConnection();
                connectionupload.setRequestMethod("POST");
                connectionupload.setRequestProperty("Authorization", uploadAuthorizationToken);
                connectionupload.setRequestProperty("Content-Type", contentType);
                connectionupload.setRequestProperty("X-Bz-File-Name", fileName);
                connectionupload.setRequestProperty("X-Bz-Content-Sha1", "do_not_verify");
                connectionupload.setDoOutput(true);
                DataOutputStream writer = new DataOutputStream(connectionupload.getOutputStream());
                writer.write(fileData);
                String jsonResponse = myInputStreamReader(connectionupload.getInputStream());


                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("fileId")){
                        String[] splitter=s.split("\"");
                        fileid=splitter[3];


                    }
                }



            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connectionupload.disconnect();
            }

            return null;
        }


    }

    public static String myInputStreamReader(InputStream in) throws IOException {
        InputStreamReader reader = new InputStreamReader(in);
        StringBuilder sb = new StringBuilder();
        int c = reader.read();
        while (c != -1) {
            sb.append((char)c);
            c = reader.read();
        }
        reader.close();


        return sb.toString();
    }


}
