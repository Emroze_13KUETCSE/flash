package com.status.fartech.socialmedia;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity{
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    RelativeLayout rootView;

    ImageView imgLang;

    ImageView imgHome,imgTrend,imgSearch,imgProfile;

    View home,search;

    LinearLayout l1,l2,l3,l4;

    RelativeLayout rlCreatePost;
    ImageView imgCreatePost;
    RelativeLayout rlRootParent;
    ImageView imgClose;

    LinearLayout llc1,llc2,llc3,llc4;



    private boolean isOpen = false;

    TabLayout tabLayout_home;

    ViewPager viewPager;

    View feed,follow,trend;


    public ArrayList<PostItem> itemArrayList = new ArrayList<>();
    public ArrayList<PostItem> itemArrayList2 = new ArrayList<>();

    Map<String, PostItem> map = new TreeMap<>();
    Map<String, PostItem> map_trend = new TreeMap<>();
    private LinearLayoutManager mLayoutManager;
    int counter = 0;
    int REQUEST_STORAGE_PERMISSIONS_CODE = 1;
    int REQUEST_STORAGE_PERMISSIONS_CODE2 = 2;

    private View currentFocusedLayout, oldFocusedLayout;
    private LinearLayoutManager layoutmanager;
    public SimpleExoPlayer exoPlayer;
    int adapterClickedPos = 0;
    ExoAdapter.CarsViewHolder carsViewHolder;
    private int height;
    ExoAdapter adapter;
    ProgressBar progressBar,progressBar_trend,progressBar_follow;
    RecyclerView recyclerView;
    JSONArray postJSON,postJSON_t;
    int progresss;

    DB_SqliteFollowing db_sqliteFollowing;
    String useridd;
    DB_SqliteFavorites db_sqliteFavorites;

    SwipeRefreshLayout mSwipeRefreshLayout,swipeRefreshLayout_follow;

    static int itemPosition = 0;

    static int itemForComment = 0;
    ExoAdapter.CarsViewHolder carsViewHolder_comments;
    String lang;





    View trendRoot;
    TabLayout tabLayout_trend;
    RecyclerView recyclerView_trend;
    TrendingAdapter2 adapter_trend;
    private LinearLayoutManager mLayoutManager_trend;
    public ArrayList<PostItem2> postItem2ArrayList_trend = new ArrayList<>();
    public ArrayList<PostItem> itemArrayList_trend = new ArrayList<>();
    public ArrayList<PostItem> itemArrayList_trend_root = new ArrayList<>();


    RecyclerView recyclerView_follow;
    private LinearLayoutManager mLayoutManager_follow;
    ExoAdapter adapter_follow;
    public ArrayList<PostItem> itemArrayList_follow = new ArrayList<>();
    public ArrayList<PostItem> itemArrayList_follow2 = new ArrayList<>();

    int counter_follow=0;
    private View currentFocusedLayout_follow, oldFocusedLayout_follow;
    private LinearLayoutManager layoutmanager_follow;
    public SimpleExoPlayer exoPlayer_follow;
    static int itemPosFollow=0;
    int adapterClickedPos_f = 0;
    ExoAdapter.CarsViewHolder carsViewHolder_f;
    JSONArray postJSON_f;
    int progresss_f;
    FirebaseAuth mAuth;

    int SELECT_PHOTO=80;
    int SELECT_VIDEO = 90;

    RelativeLayout rl1_search,rl2_search;
    ProgressBar progressBar_search;
    EditText et_search;
    public ArrayList<PostItem> itemArrayList_search = new ArrayList<>();
    ExoAdapter adapter_search;
    TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7;
    RecyclerView recyclerView_search;
    String search_text="";
    private LinearLayoutManager layoutmanager_search,layoutmanager_search2;
    public SimpleExoPlayer exoPlayer_search;
    int adapterClickedPos_search = 0;
    ExoAdapter.CarsViewHolder carsViewHolder_search;
    static int itemPosSearch=0;
    int counter_search=0;
    private View currentFocusedLayout_search, oldFocusedLayout_search;


    private static final String WHATSAPP_STATUSES_LOCATION = "/WhatsApp/Media/.Statuses";
    //public static RecyclerView friendslist2;
    ArrayList<statuseslistitem> f = new ArrayList<statuseslistitem>();// list of file paths
    File[] listFile;
    MyRecyclerViewAdapter adapter_whatsApp;

    CardView rl_whatsApp;
    SharedPreferences sharedpref;
    SharedPreferences.Editor statuseditor;
    AlertDialog alertDialog;
    File imagefile,storagedir,file;
    String picturename,mCurrentPath;
    Uri pictureuri;
    AlertDialog alertDialog2;
    public static AppBarLayout appBarLayout;
    String statusexists="no";
    ArrayList<listitemsnew> newarray = new ArrayList<listitemsnew>();
    int newposition;
    private String videoPath,languagetoload;
    private Uri video;
    //ImageView blackimage;
    int offset=8;
    int countoffset=0;
    int countofffset=0;
    int countoffset2=0;
    String showmore="yes";
    VideoView videoView;


    ArrayList<WhatsAppItem > whatsAppItemArrayList = new ArrayList<>();
    RecyclerView recyclerView_whatsApp;
    WhatsAppAdapterSmall whatsAppAdapterSmall;

    private static int firstVisibleInListview;

    private int lastY = 0;

    private boolean isUserScrolling = false;
    private boolean isListGoingUp = true;

    TextView tvEmpty_follow,tvEmpty_search;

    RecyclerView hashListView;
    HashTagAdapter hashTagAdapter;
    public ArrayList<String> hashList,hashList2;

    String hashUrl =  "http://heyapp.in/hashtags.php";

    SwipeRefreshLayout swipeRefreshLayout_trend;

    String whatsAppText = "";

    static int trendTabPos = 0;


    RecyclerView recyclerView_whatsApp_xl;
    WhatsAppAdpaterLarge whatsAppAdpaterLarge;
    RecyclerView.LayoutManager mLayoutManager_xl;
    int posOfItem_xl=0;
    SnapHelper snapHelper_xl;
    public SimpleExoPlayer exoPlayer_xl;
    RelativeLayout rl_whatsApp_xl;
    boolean isPlaying=false;

    boolean isReady = false;

    int adapterClickedPos_xl = 0;
    WhatsAppAdpaterLarge.CarsViewHolder carsViewHolder_xl;
    boolean isWhatsApp=false;






    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    RecyclerView.SmoothScroller smoothScroller;

    ImageView imgSetti;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(getApplicationContext());
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(100 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        //config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());

        trendTabPos=0;
        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        useridd = preference.getString("uid","");
        lang = preference.getString("lang","");


        db_sqliteFollowing = new DB_SqliteFollowing(getApplicationContext());
        db_sqliteFavorites = new DB_SqliteFavorites(getApplicationContext());

        rlRootParent = findViewById(R.id.root_parent);
        rootView = findViewById(R.id.root_view);
        rlCreatePost = findViewById(R.id.create_post);

        imgClose = findViewById(R.id.img_close);
        imgCreatePost = findViewById(R.id.img_create_post);

        home = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.home_pager, null, false);

        search = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.search_view, null, false);

        feed = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.feed_view, null, false);

        follow = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.follow_view, null, false);

        trend = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.trend_view, null, false);

        hashListView = search.findViewById(R.id.recycler_view_hash);



        imgSetti = home.findViewById(R.id.img_setti);

        swipeRefreshLayout_trend = trend.findViewById(R.id.swipe_refresh_layout);

        //friendslist2 = feed.findViewById(R.id.listview1);

        tvEmpty_follow = follow.findViewById(R.id.tv_empty);
        tvEmpty_search = follow.findViewById(R.id.tv_empty);

        progressBar = feed.findViewById(R.id.progress);

        recyclerView =feed.findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);


        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getApplicationContext());

        smoothScroller = new LinearSmoothScroller(getApplicationContext()) {
            @Override protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };


        recyclerView.setLayoutManager(mLayoutManager);


        recyclerView_whatsApp = feed.findViewById(R.id.listview1);
        rl_whatsApp = feed.findViewById(R.id.rl);

        et_search = search.findViewById(R.id.et_search);
        rl1_search = search.findViewById(R.id.rl1);
        rl2_search = search.findViewById(R.id.rl2);
        progressBar_search = search.findViewById(R.id.progress);

        swipeRefreshLayout_follow = follow.findViewById(R.id.swipe_refresh_layout);

        recyclerView_follow = follow.findViewById(R.id.recycler_view);

        progressBar_follow = follow.findViewById(R.id.progress);
        progressBar_trend = trend.findViewById(R.id.progress);

        rootView.addView(home);

        imgLang = home.findViewById(R.id.img_lang);
        tabLayout_home = home.findViewById(R.id.tab_layout);
        viewPager = home.findViewById(R.id.viewpager);

        imgHome = findViewById(R.id.img_home);
        imgTrend = findViewById(R.id.img_trend);
        imgSearch = findViewById(R.id.img_search);
        imgProfile = findViewById(R.id.img_profile);
        l1 = findViewById(R.id.l1);
        l2 = findViewById(R.id.l2);
        l3 = findViewById(R.id.l3);
        l4 = findViewById(R.id.l4);

        llc1 = findViewById(R.id.ll_c1);
        llc2 = findViewById(R.id.ll_c2);
        llc3 = findViewById(R.id.ll_c3);
        llc4 = findViewById(R.id.ll_c4);



        //whats app

        recyclerView_whatsApp.setAdapter(null);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView_whatsApp.setLayoutManager(linearLayoutManager);


        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new BackgroundTask().execute();
            }
        },2000);

        //firstVisibleInListview = mLayoutManager.findFirstVisibleItemPosition();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView re, int newState) {
                super.onScrollStateChanged(re, newState);




                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    int ddd = mLayoutManager.findFirstVisibleItemPosition();

                    System.out.println("only sc >> "+ddd);

                    if (statusexists.equals("yes")){

                        if(ddd == 0){

                            //rl_whatsApp.animate().translationY(550);
                            rl_whatsApp.animate().translationY(0);

                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                    RelativeLayout.LayoutParams.MATCH_PARENT,
                                    RelativeLayout.LayoutParams.MATCH_PARENT
                            );
                            params.setMargins(0, dpToPx(108), 0, 0);
                            mSwipeRefreshLayout.setLayoutParams(params);
                            mSwipeRefreshLayout.requestFocus();
                        }
                        else if(ddd >= 1) {
                            //rl_whatsApp.animate().translationY(0);

                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                    RelativeLayout.LayoutParams.MATCH_PARENT,
                                    RelativeLayout.LayoutParams.MATCH_PARENT
                            );
                            params.setMargins(0, 8, 0, 0);
                            mSwipeRefreshLayout.setLayoutParams(params);

                            rl_whatsApp.animate().translationY(dpToPx(-100));

                        }
                    }

                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


            }
        });
        //search

        tv1 = search.findViewById(R.id.tv1);
        tv2 = search.findViewById(R.id.tv2);
        tv3 = search.findViewById(R.id.tv3);
        tv4 = search.findViewById(R.id.tv4);
        tv5 = search.findViewById(R.id.tv5);
        tv6 = search.findViewById(R.id.tv6);
        tv7 = search.findViewById(R.id.tv7);
        recyclerView_search = search.findViewById(R.id.recycler_view);
        layoutmanager_search = new LinearLayoutManager(getApplicationContext());
        recyclerView_search.setLayoutManager(layoutmanager_search);

        getHash();

        if(lang.equals("hindi")){
            tv1.setText(getString(R.string.hindi_1));
            tv2.setText(getString(R.string.hindi_2));
            tv3.setText(getString(R.string.hindi_3));
            tv4.setText(getString(R.string.hindi_4));
            tv5.setText(getString(R.string.hindi_5));
            tv6.setText(getString(R.string.hindi_6));
            tv7.setText(getString(R.string.hindi_7));

            whatsAppText = getString(R.string.w_hindi);
        }
        else if(lang.equals("tamil")){
            tv1.setText(getString(R.string.tamil_1));
            tv2.setText(getString(R.string.tamil_2));
            tv3.setText(getString(R.string.tamil_3));
            tv4.setText(getString(R.string.tamil_4));
            tv5.setText(getString(R.string.tamil_5));
            tv6.setText(getString(R.string.tamil_6));
            tv7.setText(getString(R.string.tamil_7));

            whatsAppText = getString(R.string.w_tamil);
        }
        else if(lang.equals("malayalam")){
            tv1.setText(getString(R.string.mala_1));
            tv2.setText(getString(R.string.mala_2));
            tv3.setText(getString(R.string.mala_3));
            tv4.setText(getString(R.string.mala_4));
            tv5.setText(getString(R.string.mala_5));
            tv6.setText(getString(R.string.mala_6));
            tv7.setText(getString(R.string.mala_7));
            whatsAppText = getString(R.string.w_mala);
        }
        else if(lang.equals("telugu")){
            tv1.setText(getString(R.string.telegu_1));
            tv2.setText(getString(R.string.telegu_2));
            tv3.setText(getString(R.string.telegu_3));
            tv4.setText(getString(R.string.telegu_4));
            tv5.setText(getString(R.string.telegu_5));
            tv6.setText(getString(R.string.telegu_6));
            tv7.setText(getString(R.string.telegu_7));
            whatsAppText = getString(R.string.w_telegu);
        }
        else if(lang.equals("kannada")){
            tv1.setText(getString(R.string.kannada_1));
            tv2.setText(getString(R.string.kannada_2));
            tv3.setText(getString(R.string.kannada_3));
            tv4.setText(getString(R.string.kannada_4));
            tv5.setText(getString(R.string.kannada_5));
            tv6.setText(getString(R.string.kannada_6));
            tv7.setText(getString(R.string.kannada_7));
            whatsAppText = getString(R.string.w_kannada);
        }
        else if(lang.equals("bengali")){
            tv1.setText(getString(R.string.bangla_1));
            tv2.setText(getString(R.string.bangla_2));
            tv3.setText(getString(R.string.bangla_3));
            tv4.setText(getString(R.string.bangla_4));
            tv5.setText(getString(R.string.bangla_5));
            tv6.setText(getString(R.string.bangla_6));
            tv7.setText(getString(R.string.bangla_7));
            whatsAppText = getString(R.string.w_bangla);
        }

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_text = tv1.getText().toString();
                et_search.setText(search_text);
                et_search.setSelection(et_search.getText().length());
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_text = tv2.getText().toString();
                et_search.setText(search_text);
                et_search.setSelection(et_search.getText().length());
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_text = tv3.getText().toString();
                et_search.setText(search_text);
                et_search.setSelection(et_search.getText().length());
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_text = tv4.getText().toString();
                et_search.setText(search_text);
                et_search.setSelection(et_search.getText().length());
            }
        });
        tv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_text = tv5.getText().toString();
                et_search.setText(search_text);
                et_search.setSelection(et_search.getText().length());
            }
        });
        tv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_text = tv6.getText().toString();
                et_search.setText(search_text);
                et_search.setSelection(et_search.getText().length());
            }
        });
        tv7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_text = tv7.getText().toString();
                et_search.setText(search_text);
                et_search.setSelection(et_search.getText().length());
            }
        });




        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 1) {
                    final Handler handler=new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            search_text = et_search.getText().toString();
                            Search ss = new Search();
                            ss.execute();
                        }
                    },500);
                }
                else {
                    rl1_search.setVisibility(View.VISIBLE);
                    rl2_search.setVisibility(View.GONE);
                    recyclerView_search.setVisibility(View.GONE);
                }

            }
        });













        tabLayout_home.addTab(tabLayout_home.newTab().setText("Feed"));
        tabLayout_home.addTab(tabLayout_home.newTab().setText("Trend"));
        tabLayout_home.addTab(tabLayout_home.newTab().setText("Follow"));

        tabLayout_home.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();

                if(pos == 0){
                    viewPager.setCurrentItem(0,true);
                    imgHome.setImageResource(R.drawable.home2_color);
                    imgSearch.setImageResource(R.drawable.search_icon);
                    imgTrend.setImageResource(R.drawable.trend);
                    imgProfile.setImageResource(R.drawable.profile);

                }
                else if(pos == 1){
                    viewPager.setCurrentItem(1,true);
                    imgHome.setImageResource(R.drawable.home2);
                    imgSearch.setImageResource(R.drawable.search_icon);
                    imgTrend.setImageResource(R.drawable.trend_color);
                    imgProfile.setImageResource(R.drawable.profile);

                }
                else {
                    viewPager.setCurrentItem(2,true);
                    //follow();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        imgLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Language2.class);
                startActivity(intent);
            }
        });



        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgHome.setImageResource(R.drawable.home2_color);
                imgSearch.setImageResource(R.drawable.search_icon);
                imgTrend.setImageResource(R.drawable.trend);
                imgProfile.setImageResource(R.drawable.profile);

                if(rootView.getRootView() == home){

                }
                else {
                    rootView.removeAllViews();
                    rootView.addView(home);
                }

                viewPager.setCurrentItem(0,true);
                TabLayout.Tab tab = tabLayout_home.getTabAt(0);
                tab.select();

                smoothScroller.setTargetPosition(0);

                mLayoutManager.startSmoothScroll(smoothScroller);

            }
        });

        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgHome.setImageResource(R.drawable.home2);
                imgSearch.setImageResource(R.drawable.search_icon_color);
                imgTrend.setImageResource(R.drawable.trend);
                imgProfile.setImageResource(R.drawable.profile);

                rootView.removeAllViews();
                rootView.addView(search);

            }
        });
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgHome.setImageResource(R.drawable.home2);
                imgSearch.setImageResource(R.drawable.search_icon);
                imgTrend.setImageResource(R.drawable.trend_color);
                imgProfile.setImageResource(R.drawable.profile);

                if(rootView.getRootView() == home){

                }
                else {
                    rootView.removeAllViews();
                    rootView.addView(home);
                }

                viewPager.setCurrentItem(1,true);
                TabLayout.Tab tab = tabLayout_home.getTabAt(1);
                tab.select();


            }
        });
        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseUser mUser = mAuth.getCurrentUser();

                if(mUser == null){
                    LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                    logInBottomSheet.show(getSupportFragmentManager(),"main");
                }
                else {
                    Intent intent = new Intent(getApplicationContext(),Profile.class);
                    startActivity(intent);
                }

            }
        });

        imgCreatePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewMenu();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewMenu();
            }
        });



        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public boolean isViewFromObject(final View view, final Object object) {
                return view.equals(object);
            }

            @Override
            public void destroyItem(final View container, final int position, final Object object) {
                ((ViewPager) container).removeView((View) object);
            }

            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {

                if(position == 0){

                    container.addView(feed);

                    return feed;
                }
                else if(position == 1){

                    container.addView(trend);

                    return trend;
                }
                else{
                    container.addView(follow);

                    return follow;
                }

            }

        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                int pos = i;

                if(pos == 0){
                    TabLayout.Tab tab = tabLayout_home.getTabAt(0);
                    tab.select();
                    imgHome.setImageResource(R.drawable.home2_color);
                    imgSearch.setImageResource(R.drawable.search_icon);
                    imgTrend.setImageResource(R.drawable.trend);
                    imgProfile.setImageResource(R.drawable.profile);


                }
                else if(pos == 1){
                    TabLayout.Tab tab = tabLayout_home.getTabAt(1);
                    tab.select();
                    imgHome.setImageResource(R.drawable.home2);
                    imgSearch.setImageResource(R.drawable.search_icon);
                    imgTrend.setImageResource(R.drawable.trend_color);
                    imgProfile.setImageResource(R.drawable.profile);

                }
                else {

                    TabLayout.Tab tab = tabLayout_home.getTabAt(2);
                    tab.select();
                    //follow();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        viewPager.setCurrentItem(0);
        TabLayout.Tab tab = tabLayout_home.getTabAt(0);
        tab.select();

        feed();
        trendRoot = trend.findViewById(R.id.trend_root);
        tabLayout_trend = trend.findViewById(R.id.tab_layout);


        tabLayout_trend.addTab(tabLayout_trend.newTab().setText("All"));
        if(lang.equals("hindi")){
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.hindi_1)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.hindi_2)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.hindi_3)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.hindi_4)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.hindi_5)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.hindi_6)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.hindi_7)));

        }
        else if(lang.equals("tamil")){
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.tamil_1)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.tamil_2)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.tamil_3)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.tamil_4)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.tamil_5)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.tamil_6)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.tamil_7)));

        }
        else if(lang.equals("malayalam")){
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.mala_1)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.mala_2)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.mala_3)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.mala_4)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.mala_5)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.mala_6)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.mala_7)));

        }
        else if(lang.equals("telugu")){
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.telegu_1)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.telegu_2)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.telegu_3)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.telegu_4)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.telegu_5)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.telegu_6)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.telegu_7)));

        }
        else if(lang.equals("kannada")){
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.kannada_1)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.kannada_2)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.kannada_3)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.kannada_4)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.kannada_5)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.kannada_6)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.kannada_7)));

        }
        else if(lang.equals("bengali")){
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.bangla_1)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.bangla_2)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.bangla_3)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.bangla_4)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.bangla_5)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.bangla_6)));
            tabLayout_trend.addTab(tabLayout_trend.newTab().setText(getString(R.string.bangla_7)));

        }

        tabLayout_trend.setTabGravity(TabLayout.GRAVITY_FILL);

        recyclerView_trend = trend.findViewById(R.id.recycler_view);

        mLayoutManager_trend = new LinearLayoutManager(getApplicationContext());
        recyclerView_trend.setLayoutManager(mLayoutManager_trend);


        mLayoutManager_follow = new LinearLayoutManager(getApplicationContext());
        recyclerView_follow.setLayoutManager(mLayoutManager_follow);

        swipeRefreshLayout_follow.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                follow();
            }
        });


        llc4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if (currentUser==null) {
                    LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                    logInBottomSheet.show(getSupportFragmentManager(),"main");
                }
                else {
                    Intent intent=new Intent(getApplicationContext(),TextPost.class);
                    startActivity(intent);
                }
                viewMenu();

            }
        });

        llc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if (currentUser==null) {
                    LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                    logInBottomSheet.show(getSupportFragmentManager(),"main");
                }
                else {
                    Intent intent=new Intent(getApplicationContext(),CameraPost.class);
                    startActivity(intent);
                }
                viewMenu();

            }
        });

        llc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewMenu();
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if (currentUser==null) {
                    LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                    logInBottomSheet.show(getSupportFragmentManager(),"main");
                }
                else {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                }

            }

        });

        llc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewMenu();
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if (currentUser==null) {
                    LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                    logInBottomSheet.show(getSupportFragmentManager(),"main");
                }
                else {
                   /* Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("video/*");
                    startActivityForResult(photoPickerIntent, SELECT_VIDEO);
*/
                    /*Intent intent = new Intent();
                    intent.setType("video/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Video"),SELECT_VIDEO);
*/
                    Intent mediaIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                    mediaIntent.setType("video/*"); // Set MIME type as per requirement
                    startActivityForResult(mediaIntent,SELECT_VIDEO);

                }


            }
        });

        swipeRefreshLayout_trend.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                jsonParse();
            }
        });

        imgSetti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Settings.class);
                startActivity(intent);
            }
        });

        whatsAppView();
    }


    public void whatsAppView(){

        rl_whatsApp_xl=findViewById(R.id.rl_whatsApp);

        recyclerView_whatsApp_xl = findViewById(R.id.recycler_view);

        recyclerView_whatsApp_xl.setAdapter(null);

        snapHelper_xl = new PagerSnapHelper();
        snapHelper_xl.attachToRecyclerView(recyclerView_whatsApp_xl);

        mLayoutManager_xl = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager_xl.scrollToPosition(posOfItem_xl);
        recyclerView_whatsApp_xl.setLayoutManager(mLayoutManager_xl);
        recyclerView_whatsApp_xl.setItemAnimator(new DefaultItemAnimator());

    }

    WhatsAppAdpaterLarge.OnCarItemClickListener listener_whatsApp_xl = new WhatsAppAdpaterLarge.OnCarItemClickListener() {
        @Override
        public void onWhatsAppClicked(int position, WhatsAppItem item, WhatsAppAdpaterLarge.CarsViewHolder holder) {
            if(exoPlayer != null){
                if(isPlaying){
                    isPlaying = false;
                    exoPlayer.setPlayWhenReady(false);
                    exoPlayer.getPlaybackState();
                    holder.play.setVisibility(View.VISIBLE);
                }else {
                    if(isReady){
                        exoPlayer.setPlayWhenReady(true);
                        exoPlayer.getPlaybackState();
                        isPlaying = true;
                        holder.play.setVisibility(View.GONE);
                    }
                }


            }
        }

        @Override
        public void onWhatsAppllClicked(int position, WhatsAppItem item, WhatsAppAdpaterLarge.CarsViewHolder holder) {
            File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Flash/");

            boolean doSave = true;
            if (!f.exists()) {
                doSave = f.mkdirs();
            }

            copyFileOrDirectory(item.getFilePath(),f.getAbsolutePath());

            Toast.makeText(MainActivity.this, "Saved", Toast.LENGTH_SHORT).show();
        }
    };


    public static void copyFileOrDirectory(String srcDir, String dstDir) {

        try {
            File src = new File(srcDir);
            File dst = new File(dstDir, src.getName());

            if (src.isDirectory()) {

                String files[] = src.list();
                int filesLength = files.length;
                for (int i = 0; i < filesLength; i++) {
                    String src1 = (new File(src, files[i]).getPath());
                    String dst1 = dst.getPath();
                    copyFileOrDirectory(src1, dst1);

                }
            } else {
                copyFile(src, dst);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }

        }
    }


    @Override
    public void onPause() {
        super.onPause();

        System.out.println("only feed <<<<<<<<<<<<<<<<<<<< onPause "+itemPosition);

        if(!whatsAppItemArrayList.isEmpty()){

            if (exoPlayer != null){
                exoPlayer.setPlayWhenReady(false);
                carsViewHolder_xl.play.setVisibility(View.VISIBLE);
                carsViewHolder_xl.imgCover.setVisibility(View.VISIBLE);
                carsViewHolder_xl.mExoPlayer.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!itemArrayList2.isEmpty()){
            mLayoutManager.scrollToPosition(itemPosition);
        }

        if(!itemArrayList_follow2.isEmpty()){
            mLayoutManager_follow.scrollToPosition(itemPosFollow);
        }

        if(!itemArrayList_search.isEmpty()){
            layoutmanager_search.scrollToPosition(itemPosFollow);
        }

    }


    class Search extends AsyncTask<Void,Void,Void> {
        byte[] pic;
        Bitmap bmp;

        public void setByte(byte[] bytes){
            this.pic=bytes;
        }


        @Override
        protected Void doInBackground(Void... voids) {



            for(int i = 0 ; i < itemArrayList.size();i++){
                PostItem item = itemArrayList.get(i);

                String str = item.getText().toLowerCase();


                if(str.contains(search_text.toLowerCase())){
                    itemArrayList_search.add(item);
                }
            }





            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            itemArrayList_search.clear();

            rl2_search.setVisibility(View.VISIBLE);
        }




        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            rl1_search.setVisibility(View.GONE);
            rl2_search.setVisibility(View.GONE);

            if(itemArrayList_search.size() != 0){
                tvEmpty_search.setVisibility(View.GONE);

                recyclerView_search.setVisibility(View.VISIBLE);

                adapter_search = new ExoAdapter(itemArrayList_search,getApplicationContext());
                adapter_search.setOnCarItemClickListener(listener_search);
                recyclerView_search.setAdapter(adapter_search);

                recyclerView_search.addOnScrollListener(new RecyclerView.OnScrollListener() {

                    @Override
                    public void onScrollStateChanged(final RecyclerView recyclerView, int scrollState) {


                        if (scrollState == RecyclerView.SCROLL_STATE_IDLE) {

                            layoutmanager_search2 = (LinearLayoutManager)recyclerView.getLayoutManager();

                            int firstVisiblePosition = layoutmanager_search2.findFirstCompletelyVisibleItemPosition();

                            int ddd = layoutmanager_search2.findFirstVisibleItemPosition();
                            int eee = layoutmanager_search2.findLastCompletelyVisibleItemPosition();
                            int fff = layoutmanager_search2.findLastVisibleItemPosition();

                            System.out.println("only ======================================= "+firstVisiblePosition);
                            System.out.println("only 1st full visible "+firstVisiblePosition);
                            System.out.println("only 1st partial visible "+ddd);
                            System.out.println("only last full visible "+eee);
                            System.out.println("only last partial visible "+fff);

                            if(ddd >= 0){
                                itemPosSearch = ddd;
                                System.out.println("only f 1st partial visible "+itemPosFollow);

                            }
                            else {
                                itemPosSearch=0;
                            }
                            if (firstVisiblePosition >= 0 || fff >= 0)
                            {
                                if (oldFocusedLayout_search != null)
                                {
                                    System.out.println("only remove "+firstVisiblePosition);
                                    if(exoPlayer_search != null){

                                        carsViewHolder_search.play.setVisibility(View.VISIBLE);
                                        carsViewHolder_search.imgCover.setVisibility(View.VISIBLE);
                                        carsViewHolder_search.mExoPlayer.setVisibility(View.GONE);

                                    }
                                }


                            }
                            if(exoPlayer_search != null){

                                exoPlayer_search.setPlayWhenReady(false);
                            }

                            currentFocusedLayout_search = layoutmanager_search2.findViewByPosition(firstVisiblePosition);
                            //VideoView vv_dashboard = (VideoView)currentFocusedLayout.findViewById(R.id.vv_dashboard);
                            ////to play video of selected recylerview, videosData is an array-list which is send to recyclerview adapter to fill the view. Here we getting that specific video which is displayed through recyclerview.
                            //playVideo(videosData.get(positionView));

                            oldFocusedLayout_search = currentFocusedLayout_search;




                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    }
                });

            }else {
                tvEmpty_search.setVisibility(View.VISIBLE);
            }

        }
    }


    ExoAdapter.OnCarItemClickListener listener_search = new ExoAdapter.OnCarItemClickListener() {
        @Override
        public void onllClicked(final int position, PostItem item, final ExoAdapter.CarsViewHolder holder) {


            System.out.println("only  >>>>>>>>> "+holder.imgCover.getHeight()+" "+holder.imgCover.getWidth());

            /*if (!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;



                holder.cardmore.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.play.setVisibility(View.GONE);

                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelection.Factory videoTrackSelectionFactory =
                        new AdaptiveTrackSelection.Factory(bandwidthMeter);
                TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

                if(exoPlayer != null){
                    exoPlayer.setPlayWhenReady(false);
                    exoPlayer.stop();
                }

                exoPlayer_search = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

                holder.mExoPlayer.setPlayer(exoPlayer_search);



                exoPlayer_search.addListener(new Player.EventListener() {
                    @Override
                    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                        System.out.println("only 1 "+position);
                    }

                    @Override
                    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                        System.out.println("only 2 "+position);
                    }

                    @Override
                    public void onLoadingChanged(boolean isLoading) {
                        System.out.println("only 3 "+position);
                    }

                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                        if (playWhenReady && playbackState == Player.STATE_READY) {
                            System.out.println("only  playing  "+position);
                            // media actually playing
                            holder.progressBar.setVisibility(View.GONE);
                            holder.play.setVisibility(View.GONE);
                            holder.imgCover.setVisibility(View.GONE);
                            holder.mExoPlayer.setVisibility(View.VISIBLE);
                            holder.mExoPlayer.getLayoutParams().height = holder.imgCover.getHeight();
                            holder.mExoPlayer.requestFocus();


                        } else if (playWhenReady) {
                            // might be idle (plays after prepare()),
                            // buffering (plays when data available)
                            // or ended (plays when seek away from end)
                            if (playbackState==Player.STATE_ENDED){
                                System.out.println("only  Ended  "+position);
                            }
                            else if (playbackState==Player.STATE_BUFFERING){
                                System.out.println("only Buffering "+position);
                            }
                            else if (playbackState==Player.STATE_IDLE){
                                System.out.println("only Idle "+position);
                            }
                        } else {
                            // player paused in any state
                            System.out.println("only  paused  "+position);



                            //horizontalCarList.get(position).setPlayWhenReady(true);

                        }

                    }

                    @Override
                    public void onRepeatModeChanged(int repeatMode) {

                    }

                    @Override
                    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                    }

                    @Override
                    public void onPlayerError(ExoPlaybackException error) {

                    }

                    @Override
                    public void onPositionDiscontinuity(int reason) {

                    }

                    @Override
                    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                    }

                    @Override
                    public void onSeekProcessed() {

                    }
                });


                CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

                MediaSource audioSource = new ExtractorMediaSource(Uri.parse(itemArrayList_search.get(position).getVideoUrl()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


                exoPlayer_search.seekTo(position, 0);

                exoPlayer_search.prepare(audioSource, true, false);


                exoPlayer_search.setPlayWhenReady(true);


                adapterClickedPos_search=position;
                carsViewHolder_search = holder;

            }else {
                holder.play.setVisibility(View.GONE);

            }*/



            //System.out.println("only ??????????????????????????????????????? select "+position+itemArrayList2.get(position).getPlayWhenReady());
            //adapter.setItemlist(itemArrayList2);
            //adapter.notifyDataSetChanged();

            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                holder.play.setVisibility(View.GONE);
            }else {


                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList_search.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList_search.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);


                float views = 0;
                String vvv="";
                views = itemArrayList_search.get(position).getViews();
                views=views+1;
                itemArrayList_search.get(position).setViews((int)views);

                if(views<=999){
                    int vvvvv = (int)views;
                    vvv = String.valueOf(vvvvv)+" views";
                }
                else if(views >= 1000){
                    views = views/1000;
                    vvv = String.valueOf(views)+"k views";
                }else if(views >= 1000000){
                    views = views/1000000;
                    vvv = String.valueOf(views)+"M views";
                }

                holder.tvViews.setText(vvv);

            }


            itemPosFollow=position;
        }

        @Override
        public void onCoverClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            System.out.println("only ??????????????????????????????????????? select "+position);

            itemPosFollow=position;
            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                Intent intent = new Intent(getApplicationContext(),ImageDisplay.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("postId", item.getPostid());
                intent.putExtra("imgUrl", item.getPpurl());
                intent.putExtra("userName", item.getUsername());
                intent.putExtra("otherId", item.getUid());
                intent.putExtra("coverUrl", item.getImagelocation());
                intent.putExtra("likes", item.getLikes());
                intent.putExtra("comments", item.getCommentsno());
                intent.putExtra("shares", item.getShares());
                intent.putExtra("downloads", item.getDownloadsno());
                intent.putExtra("txt", item.getText());
                intent.putExtra("views", item.getViews());

                startActivity(intent);
            }else {
                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList_search.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList_search.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);
            }


            float views = 0;
            String vvv="";
            views = itemArrayList_search.get(position).getViews();
            views=views+1;
            itemArrayList_search.get(position).setViews((int)views);

            if(views<=999){
                int vvvvv = (int)views;
                vvv = String.valueOf(vvvvv)+" views";
            }
            else if(views >= 1000){
                views = views/1000;
                vvv = String.valueOf(views)+"k views";
            }else if(views >= 1000000){
                views = views/1000000;
                vvv = String.valueOf(views)+"M views";
            }

            holder.tvViews.setText(vvv);


        }

        @Override
        public void onWhatsAppClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            if (!itemArrayList_search.get(position).getTime().contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList_search.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList_search.get(position).getText()+"\n"+whatsAppText);
                }
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Title", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }


                int shar = itemArrayList_search.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                itemArrayList_search.get(position).setShares(shar);

                adapter_search.setItemlist(itemArrayList_search);


                StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                            }
                        }

                        , new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("postid", itemArrayList_search.get(position).getPostid());

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringrequest);

            }else {


                int shar = itemArrayList_search.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                String[] splitter =itemArrayList_search.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }


                downloadvideoWhatsAppSearch downloadvideo2=new downloadvideoWhatsAppSearch();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList_search.get(position).getVideoUrl(),time2,String.valueOf(position));

            }
        }

        @Override
        public void onLikeClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }else {
                viewHolder.imgLike.setSelected(!viewHolder.imgLike.isSelected());
                int count = db_sqliteFavorites.get_check_List_Favorite(itemArrayList_search.get(position).getPostid());
                if (count == 0) {
                    viewHolder.imgLike.setImageResource(R.drawable.love2);
                    int lik = itemArrayList_search.get(position).getLikes();

                    itemArrayList_search.get(position).setLikes(lik+1);

                    adapter_search.setItemlist(itemArrayList_search);

                    itemArrayList_search.get(position).setLiked(true);

                    adapter_search.setItemlist3(true,position);

                    db_sqliteFavorites.Inserttoalertlist(itemArrayList_search.get(position).getPostid());

                    viewHolder.tvLikes.setText(String.valueOf(lik + 1));

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/sociallikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList_search.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);
                } else {
                    viewHolder.imgLike.setImageResource(R.drawable.dislike);
                    db_sqliteFavorites.Delete(itemArrayList_search.get(position).getPostid());

                    int lik = itemArrayList_search.get(position).getLikes();

                    itemArrayList_search.get(position).setLikes(lik-1);

                    adapter_search.setItemlist(itemArrayList_search);

                    itemArrayList_search.get(position).setLiked(false);

                    adapter_search.setItemlist3(false,position);

                    if (lik == 0) {
                        viewHolder.tvLikes.setText("Like");
                    }

                    viewHolder.tvLikes.setText(String.valueOf(lik - 1));

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialdislikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList_search.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
            }
        }

        @Override
        public void onCommentsClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                itemForComment = position;
                carsViewHolder_comments = holder;
                Intent intent=new Intent(getApplicationContext(),Comments.class);
                intent.putExtra("postid",itemArrayList_search.get(position).getPostid());
                startActivityForResult(intent,11);
            }
        }

        @Override
        public void onDownloadsClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            viewHolder.progressBar_down.setVisibility(View.VISIBLE);

            if (!itemArrayList_search.get(position).getTime().contains("/")){


                try{
                    Bitmap bm = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();

                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Flash/");

                    boolean doSave = true;
                    if (!f.exists()) {
                        doSave = f.mkdirs();
                    }

                    if (doSave) {
                        saveBitmapToFile(f,itemArrayList_search.get(position).getPostid()+".png",bm,Bitmap.CompressFormat.PNG,100);
                    }
                    else {
                        Log.e("app","Couldn't create target directory.");
                    }


                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                    int down = itemArrayList_search.get(position).getDownloadsno();
                    itemArrayList_search.get(position).setDownloadsno(down+1);
                    viewHolder.tvDownloads.setText(String.valueOf(down+1));
                    adapter_search.setItemlist(itemArrayList_search);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {




                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("postid",itemArrayList_search.get(position).postid);

                            return params;
                        }
                    };

                    RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                }catch (NullPointerException e){

                }




            }else {

                String[] splitter =itemArrayList_search.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                int down = itemArrayList_search.get(position).getDownloadsno();
                viewHolder.tvDownloads.setText(String.valueOf(down+1));

                downloadvideoooSearch downloadvideo2=new downloadvideoooSearch();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList_search.get(position).getVideoUrl(),time2,String.valueOf(position));





            }





        }

        @Override
        public void onMoreClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {

            if(holder.cardmore.getVisibility() == View.VISIBLE){
                holder.cardmore.animate().alpha(0);
                holder.cardmore.setVisibility(View.GONE);
            }else {
                holder.cardmore.setVisibility(View.VISIBLE);
                holder.cardmore.animate().alpha(1);
            }

        }

        @Override
        public void onShareClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            if (!itemArrayList_search.get(position).time.contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Image", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

            }else {

                String[] splitter =itemArrayList_search.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                downloadvideoshareSearch downloadvideo=new downloadvideoshareSearch();
                downloadvideo.setdownloadlayout(viewHolder.ll_share);
                downloadvideo.setProgressbar(viewHolder.progressBar_share);
                downloadvideo.execute(itemArrayList_search.get(position).getVideoUrl(), time2,String.valueOf(position));

            }

        }

        @Override
        public void onReportClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            Toast.makeText(getApplicationContext(), "Report Sent", Toast.LENGTH_SHORT).show();
            viewHolder.cardmore.animate().alpha(0);
            viewHolder.cardmore.setVisibility(View.GONE);
        }

        @Override
        public void onAddClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();

            FirebaseUser mUser = mAuth.getCurrentUser();

            if(mUser == null){
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                if (viewHolder.tvAdd.getText().toString().equals("Follow")) {
                    db_sqliteFollowing.Inserttoalertlist(itemArrayList_search.get(position).getUid());
                    viewHolder.tvAdd.setText("Unfollow");

                    adapter_search.setItemlist2(itemArrayList_search.get(position).getUid(),true);

                    follow();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/followings.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList_search.get(position).getUid());


                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }else {
                    db_sqliteFollowing.Delete(itemArrayList_search.get(position).uid);
                    viewHolder.tvAdd.setText("Follow");

                    adapter_search.setItemlist2(itemArrayList_search.get(position).getUid(),false);

                    follow();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/unfollow.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList_search.get(position).getUid());


                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }


            }
        }

        @Override
        public void onProPicClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {

            if(useridd.equals(item.getUid())){
                Intent intent = new Intent(getApplicationContext(),Profile.class);
                startActivity(intent);

            }else {
                Intent intent = new Intent(getApplicationContext(),OtherUserProfile.class);
                intent.putExtra("uid",item.getUid());
                intent.putExtra("userName",item.getUsername());
                intent.putExtra("imgUrl",item.getPpurl());
                startActivity(intent);
            }
        }
    };

    class InsertFollow extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {


            for (int i = 0; i < itemArrayList.size();i++){
                PostItem item = itemArrayList.get(i);

                int count3 = db_sqliteFollowing.get_check_List_Favorite(item.getUid());

                if(count3 != 0){
                    item.setFollowing(true);
                    itemArrayList_follow.add(item);
                }

            }

            Collections.shuffle( itemArrayList_follow);


            if(itemArrayList_follow2.size() == 0){
                counter_follow = 0;
                for(int i = counter_follow ; i < counter_follow+10; i++){
                    if(i == itemArrayList_follow.size()){
                        break;
                    }
                    itemArrayList_follow.get(i).setPrepared(true);
                    itemArrayList_follow2.add(itemArrayList_follow.get(i));
                }

            }


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBar_follow.setVisibility(View.VISIBLE);


        }




        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressBar_follow.setVisibility(View.GONE);



            if(itemArrayList_follow.size() != 0){
                tvEmpty_follow.setVisibility(View.GONE);
            }
            else {
                tvEmpty_follow.setVisibility(View.VISIBLE);
            }


            swipeRefreshLayout_follow.setRefreshing(false);

            counter_follow=counter_follow+10;

            adapter_follow = new ExoAdapter(itemArrayList_follow2,getApplicationContext());
            adapter_follow.setOnCarItemClickListener(listener_follow);


            recyclerView_follow.setAdapter(adapter_follow);

            recyclerView_follow.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(final RecyclerView recyclerView, int scrollState) {

                    if (!recyclerView.canScrollVertically(1)) {

                        progressBar_follow.setVisibility(View.VISIBLE);
                        final Handler handler=new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                progressBar_follow.setVisibility(View.GONE);


                                if(counter_follow <= itemArrayList_follow.size()){
                                    for(int i = counter_follow ; i < counter_follow+10; i++){
                                        if(i == itemArrayList_follow.size()){
                                            System.out.println("only size"+itemArrayList_follow.size());
                                            break;
                                        }
                                        else {
                                            itemArrayList_follow2.add(itemArrayList_follow.get(i));
                                        }

                                    }

                                    counter_follow=counter_follow+10;

                                    adapter_follow.setItemlist(itemArrayList_follow2);
                                    adapter_follow.notifyDataSetChanged();
                                }


                            }
                        },500);
                    }

                    if (scrollState == RecyclerView.SCROLL_STATE_IDLE) {

                        layoutmanager_follow = (LinearLayoutManager)recyclerView.getLayoutManager();

                        int firstVisiblePosition = layoutmanager_follow.findFirstCompletelyVisibleItemPosition();

                        int ddd = layoutmanager_follow.findFirstVisibleItemPosition();
                        int eee = layoutmanager_follow.findLastCompletelyVisibleItemPosition();
                        int fff = layoutmanager_follow.findLastVisibleItemPosition();

                        System.out.println("only ======================================= "+firstVisiblePosition);
                        System.out.println("only 1st full visible "+firstVisiblePosition);
                        System.out.println("only 1st partial visible "+ddd);
                        System.out.println("only last full visible "+eee);
                        System.out.println("only last partial visible "+fff);

                        if(ddd >= 0){
                            itemPosFollow = ddd;
                            System.out.println("only f 1st partial visible "+itemPosFollow);

                        }
                        else {
                            itemPosFollow=0;
                        }
                        if (firstVisiblePosition >= 0 || fff >= 0)
                        {
                            if (oldFocusedLayout_follow != null)
                            {
                                System.out.println("only remove "+firstVisiblePosition);
                                if(exoPlayer_follow != null){

                                    carsViewHolder_f.play.setVisibility(View.VISIBLE);
                                    carsViewHolder_f.imgCover.setVisibility(View.VISIBLE);
                                    carsViewHolder_f.mExoPlayer.setVisibility(View.GONE);

                                }
                            }


                        }
                        if(exoPlayer_follow != null){

                            exoPlayer_follow.setPlayWhenReady(false);
                        }

                        currentFocusedLayout_follow = layoutmanager_follow.findViewByPosition(firstVisiblePosition);
                        //VideoView vv_dashboard = (VideoView)currentFocusedLayout.findViewById(R.id.vv_dashboard);
                        ////to play video of selected recylerview, videosData is an array-list which is send to recyclerview adapter to fill the view. Here we getting that specific video which is displayed through recyclerview.
                        //playVideo(videosData.get(positionView));

                        oldFocusedLayout_follow = currentFocusedLayout_follow;




                    }
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                }
            });

        }
    }


    public void follow(){
        itemArrayList_follow.clear();
        itemArrayList_follow2.clear();




        new InsertFollow().execute();




    }

    ExoAdapter.OnCarItemClickListener listener_follow = new ExoAdapter.OnCarItemClickListener() {
        @Override
        public void onllClicked(final int position, PostItem item, final ExoAdapter.CarsViewHolder holder) {


            System.out.println("only  >>>>>>>>> "+holder.imgCover.getHeight()+" "+holder.imgCover.getWidth());

            /*DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;



            holder.cardmore.setVisibility(View.GONE);
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.play.setVisibility(View.GONE);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

            if(exoPlayer_follow != null){
                exoPlayer_follow.setPlayWhenReady(false);
                exoPlayer_follow.stop();
            }

            exoPlayer_follow = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

            holder.mExoPlayer.setPlayer(exoPlayer_follow);



            exoPlayer_follow.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                    System.out.println("only 1 "+position);
                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                    System.out.println("only 2 "+position);
                }

                @Override
                public void onLoadingChanged(boolean isLoading) {
                    System.out.println("only 3 "+position);
                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                    if (playWhenReady && playbackState == Player.STATE_READY) {
                        System.out.println("only  playing  "+position);
                        // media actually playing
                        holder.progressBar.setVisibility(View.GONE);
                        holder.play.setVisibility(View.GONE);
                        holder.imgCover.setVisibility(View.GONE);
                        holder.mExoPlayer.setVisibility(View.VISIBLE);
                        holder.mExoPlayer.getLayoutParams().height = holder.imgCover.getHeight();
                        holder.mExoPlayer.requestFocus();


                    } else if (playWhenReady) {
                        // might be idle (plays after prepare()),
                        // buffering (plays when data available)
                        // or ended (plays when seek away from end)
                        if (playbackState==Player.STATE_ENDED){
                            System.out.println("only  Ended  "+position);
                        }
                        else if (playbackState==Player.STATE_BUFFERING){
                            System.out.println("only Buffering "+position);
                        }
                        else if (playbackState==Player.STATE_IDLE){
                            System.out.println("only Idle "+position);
                        }
                    } else {
                        // player paused in any state
                        System.out.println("only  paused  "+position);



                        //horizontalCarList.get(position).setPlayWhenReady(true);

                    }

                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {

                }
            });


            CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

            MediaSource audioSource = new ExtractorMediaSource(Uri.parse(itemArrayList_follow2.get(position).getVideoUrl()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


            exoPlayer_follow.seekTo(position, 0);

            exoPlayer_follow.prepare(audioSource, true, false);

            System.out.println("only  here  "+itemArrayList_follow2.get(position).getPrepared());

            exoPlayer_follow.setPlayWhenReady(true);


            adapterClickedPos_f = position;
            carsViewHolder_f = holder;*/


            //System.out.println("only ??????????????????????????????????????? select "+position+itemArrayList2.get(position).getPlayWhenReady());
            //adapter.setItemlist(itemArrayList2);
            //adapter.notifyDataSetChanged();

            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                holder.play.setVisibility(View.GONE);
            }else {


                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList_follow.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList_follow.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);

                float views = 0;
                String vvv="";
                views = itemArrayList_follow.get(position).getViews();
                views=views+1;
                itemArrayList_follow.get(position).setViews((int)views);

                if(views<=999){
                    int vvvvv = (int)views;
                    vvv = String.valueOf(vvvvv)+" views";
                }
                else if(views >= 1000){
                    views = views/1000;
                    vvv = String.valueOf(views)+"k views";
                }else if(views >= 1000000){
                    views = views/1000000;
                    vvv = String.valueOf(views)+"M views";
                }

                holder.tvViews.setText(vvv);

            }

            itemPosFollow=position;
        }

        @Override
        public void onCoverClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {


            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                Intent intent = new Intent(getApplicationContext(),ImageDisplay.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("postId", item.getPostid());
                intent.putExtra("imgUrl", item.getPpurl());
                intent.putExtra("userName", item.getUsername());
                intent.putExtra("otherId", item.getUid());
                intent.putExtra("coverUrl", item.getImagelocation());
                intent.putExtra("likes", item.getLikes());
                intent.putExtra("comments", item.getCommentsno());
                intent.putExtra("shares", item.getShares());
                intent.putExtra("downloads", item.getDownloadsno());
                intent.putExtra("txt", item.getText());
                intent.putExtra("views", item.getViews());

                startActivity(intent);
            }else {
                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList_follow.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList_follow.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);
            }

            float views = 0;
            String vvv="";
            views = itemArrayList_follow.get(position).getViews();
            views=views+1;
            itemArrayList_follow.get(position).setViews((int)views);

            if(views<=999){
                int vvvvv = (int)views;
                vvv = String.valueOf(vvvvv)+" views";
            }
            else if(views >= 1000){
                views = views/1000;
                vvv = String.valueOf(views)+"k views";
            }else if(views >= 1000000){
                views = views/1000000;
                vvv = String.valueOf(views)+"M views";
            }

            holder.tvViews.setText(vvv);


            itemPosFollow=position;
        }

        @Override
        public void onWhatsAppClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            if (!itemArrayList_follow2.get(position).getTime().contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList_follow2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList_follow2.get(position).getText()+"\n"+whatsAppText);
                }
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Title", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }


                int shar = itemArrayList_follow2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                itemArrayList_follow2.get(position).setShares(shar);
                itemArrayList_follow.get(position).setShares(shar);
                adapter_follow.setItemlist(itemArrayList_follow2);

                int  main_pos = itemArrayList.indexOf(itemArrayList_follow.get(position));

                itemArrayList.get(main_pos).setShares(shar);
                itemArrayList2.get(main_pos).setShares(shar);
                adapter.setItemlist(itemArrayList2);

                StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                            }
                        }

                        , new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("postid", itemArrayList_follow2.get(position).getPostid());

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringrequest);

            }else {


                String[] splitter =itemArrayList_follow2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                int shar = itemArrayList_follow2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));


                int  main_pos = itemArrayList.indexOf(itemArrayList_follow.get(position));

                itemArrayList.get(main_pos).setShares(shar);
                itemArrayList2.get(main_pos).setShares(shar);
                adapter.setItemlist(itemArrayList2);


                downloadvideoWhatsAppFollow downloadvideo2=new downloadvideoWhatsAppFollow();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList_follow2.get(position).getVideoUrl(),time2,String.valueOf(position));

            }
        }

        @Override
        public void onLikeClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }else {
                viewHolder.imgLike.setSelected(!viewHolder.imgLike.isSelected());
                int count = db_sqliteFavorites.get_check_List_Favorite(itemArrayList_follow2.get(position).getPostid());
                if (count == 0) {
                    viewHolder.imgLike.setImageResource(R.drawable.love2);
                    int lik = itemArrayList_follow2.get(position).getLikes();
                    viewHolder.tvLikes.setText(String.valueOf(lik + 1));
                    itemArrayList_follow2.get(position).setLikes(lik+1);
                    itemArrayList_follow.get(position).setLikes(lik+1);
                    adapter_follow.setItemlist(itemArrayList_follow2);

                    itemArrayList_follow.get(position).setLiked(true);
                    itemArrayList_follow2.get(position).setLiked(true);

                    adapter_follow.setItemlist3(true,position);

                    db_sqliteFavorites.Inserttoalertlist(itemArrayList_follow2.get(position).getPostid());

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/sociallikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList_follow2.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);
                } else {
                    viewHolder.imgLike.setImageResource(R.drawable.dislike);
                    db_sqliteFavorites.Delete(itemArrayList_follow2.get(position).getPostid());

                    int lik = itemArrayList_follow2.get(position).getLikes();
                    viewHolder.tvLikes.setText(String.valueOf(lik - 1));
                    itemArrayList_follow2.get(position).setLikes(lik-1);
                    itemArrayList_follow.get(position).setLikes(lik-1);

                    adapter_follow.setItemlist(itemArrayList_follow2);

                    itemArrayList_follow.get(position).setLiked(false);
                    itemArrayList_follow2.get(position).setLiked(false);
                    adapter_follow.setItemlist3(false,position);

                    if (lik == 0) {
                        viewHolder.tvLikes.setText("Like");
                    }

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialdislikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList_follow2.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
            }
        }

        @Override
        public void onCommentsClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                itemForComment = position;
                carsViewHolder_comments = holder;
                Intent intent=new Intent(getApplicationContext(),Comments.class);
                intent.putExtra("postid",itemArrayList_follow2.get(position).getPostid());
                startActivityForResult(intent,11);
            }

        }

        @Override
        public void onDownloadsClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            viewHolder.progressBar_down.setVisibility(View.VISIBLE);

            if (!itemArrayList_follow2.get(position).getTime().contains("/")){


                try{
                    Bitmap bm = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();

                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Flash/");

                    boolean doSave = true;
                    if (!f.exists()) {
                        doSave = f.mkdirs();
                    }

                    if (doSave) {
                        saveBitmapToFile(f,itemArrayList_follow2.get(position).getPostid()+".png",bm,Bitmap.CompressFormat.PNG,100);
                    }
                    else {
                        Log.e("app","Couldn't create target directory.");
                    }


                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                    int down = itemArrayList_follow2.get(position).getDownloadsno();

                    viewHolder.tvDownloads.setText(String.valueOf(down+1));
                    itemArrayList_follow2.get(position).setDownloadsno(down+1);
                    itemArrayList_follow.get(position).setDownloadsno(down+1);
                    adapter_follow.setItemlist(itemArrayList_follow2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {




                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("postid",itemArrayList_follow2.get(position).postid);

                            return params;
                        }
                    };

                    RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                }catch (NullPointerException e){

                }




            }else {

                String[] splitter =itemArrayList_follow2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                int down = itemArrayList_follow2.get(position).getDownloadsno();

                viewHolder.tvDownloads.setText(String.valueOf(down+1));

                downloadvideoooFollow downloadvideo2=new downloadvideoooFollow();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList_follow2.get(position).getVideoUrl(),time2,String.valueOf(position));





            }





        }

        @Override
        public void onMoreClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {

            if(holder.cardmore.getVisibility() == View.VISIBLE){
                holder.cardmore.animate().alpha(0);
                holder.cardmore.setVisibility(View.GONE);
            }else {
                holder.cardmore.setVisibility(View.VISIBLE);
                holder.cardmore.animate().alpha(1);
            }

        }

        @Override
        public void onShareClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            if (!itemArrayList_follow2.get(position).time.contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Image", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

            }else {
                String[] splitter =itemArrayList_follow2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                downloadvideoshareFollow downloadvideo=new downloadvideoshareFollow();
                downloadvideo.setdownloadlayout(viewHolder.ll_share);
                downloadvideo.setProgressbar(viewHolder.progressBar_share);
                downloadvideo.execute(itemArrayList_follow2.get(position).getVideoUrl(), time2,String.valueOf(position));

            }

        }

        @Override
        public void onReportClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            Toast.makeText(getApplicationContext(), "Report Sent", Toast.LENGTH_SHORT).show();
            viewHolder.cardmore.animate().alpha(0);
            viewHolder.cardmore.setVisibility(View.GONE);
        }

        @Override
        public void onAddClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();

            FirebaseUser mUser = mAuth.getCurrentUser();

            if(mUser == null){
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                if (viewHolder.tvAdd.getText().toString().equals("Follow")) {
                    db_sqliteFollowing.Inserttoalertlist(itemArrayList_follow2.get(position).getUid());
                    viewHolder.tvAdd.setText("Unfollow");

                    adapter_follow.setItemlist2(itemArrayList_follow2.get(position).getUid(),true);


                    //adapter.setItemlist2(itemArrayList_follow2.get(position).getUid(),true);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/followings.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList_follow2.get(position).getUid());


                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }else {
                    db_sqliteFollowing.Delete(itemArrayList_follow2.get(position).uid);
                    viewHolder.tvAdd.setText("Follow");

                    adapter_follow.setItemlist2(itemArrayList_follow2.get(position).getUid(),false);

                    /*int pos = itemArrayList.indexOf(item);

                    itemArrayList.get(pos).setFollowing(true);
                    itemArrayList2.get(pos).setFollowing(true);*/
                    //adapter.setItemlist2(itemArrayList_follow2.get(position).getUid(),true);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/unfollow.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList_follow2.get(position).getUid());


                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }



            }
        }

        @Override
        public void onProPicClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            if(useridd.equals(item.getUid())){
                Intent intent = new Intent(getApplicationContext(),Profile.class);
                startActivity(intent);

            }else {
                Intent intent = new Intent(getApplicationContext(),OtherUserProfile.class);
                intent.putExtra("uid",item.getUid());
                intent.putExtra("userName",item.getUsername());
                intent.putExtra("imgUrl",item.getPpurl());
                startActivity(intent);
            }

        }
    };


    String strText="";

    class ShareComparator implements Comparator<PostItem> {
        public int compare(PostItem item1, PostItem item2) {

            int whats1 = item1.getShares();
            int down1 = item1.getDownloadsno();

            int total1 = whats1+down1;

            int whats2 = item2.getShares();
            int down2 = item2.getDownloadsno();

            int total2 = whats2+down2;

            return total1 - total2;
        }
    }

    public void trend(){

        swipeRefreshLayout_trend.setRefreshing(false);
        progressBar_trend.setVisibility(View.GONE);

        itemArrayList_trend_root.clear();
        itemArrayList_trend.clear();
        ArrayList<PostItem> aaaa = new ArrayList<>();

        for(int i = 0 ; i < itemArrayList.size();i++){
            PostItem item = itemArrayList.get(i);

            if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){

                int whats = item.getShares();
                int down = item.getDownloadsno();

                int total = whats+down;

                String no_rank = String.valueOf(total);

                //map_trend.put(no_rank,item);
                aaaa.add(item);

                System.out.println("only all keys are: " + item.getUsername());
                System.out.println("only all keys are: " + no_rank);
            }

        }

        //Collections.sort(aaaa,new ShareComparator());

        Collections.shuffle(aaaa);


        System.out.println("only all====================================");
        System.out.println("only all ========================================= " );

        if(aaaa.size() != 0){
            for(int i = aaaa.size()-1; i > -1; i--){
                System.out.println("only all keys are: " + aaaa.get(i).getUsername());

                itemArrayList_trend_root.add(aaaa.get(i));
            }

        }

        if(trendTabPos == 0){
            itemArrayList_trend.clear();
            for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                PostItem item = itemArrayList_trend_root.get(i);

                if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                    itemArrayList_trend.add(item);
                }

            }

        }
        else if(trendTabPos == 1){
            if(lang.equals("hindi")){
                strText = "सिनेमा";
            }
            else if(lang.equals("tamil")){
                strText = "சினிமா";
            }
            else if(lang.equals("malayalam")){
                strText = "സിനിമ";
            }
            else if(lang.equals("telugu")){
                strText = "సినిమా";
            }
            else if(lang.equals("kannada")){
                strText = "ಸಿನೆಮಾ";
            }
            else if(lang.equals("bengali")){
                strText = "সিনেমা";
            }

            itemArrayList_trend.clear();
            for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                PostItem item = itemArrayList_trend_root.get(i);

                String txt = item.getText();
                if(txt.contains(strText)){
                    if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                        itemArrayList_trend.add(item);
                    }

                }

            }

        }
        else if(trendTabPos == 2){
            if(lang.equals("hindi")){
                strText = "खेल";
            }
            else if(lang.equals("tamil")){
                strText = "விளையாட்டு";
            }
            else if(lang.equals("malayalam")){
                strText = "ക്രിക്കറ്റ്";
            }
            else if(lang.equals("telugu")){
                strText = "క్రికెట్";
            }
            else if(lang.equals("kannada")){
                strText = "ಕ್ರಿಕೆಟ್";
            }
            else if(lang.equals("bengali")){
                strText = "ক্রিকেট";
            }

            itemArrayList_trend.clear();
            for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                PostItem item = itemArrayList_trend_root.get(i);

                String txt = item.getText();
                if(txt.contains(strText)){
                    if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                        itemArrayList_trend.add(item);
                    }

                }

            }
        }
        else if(trendTabPos == 3){
            if(lang.equals("hindi")){
                strText = "क्रिकेट";
            }
            else if(lang.equals("tamil")){
                strText = "கிரிக்கெட்";
            }
            else if(lang.equals("malayalam")){
                strText = "സ്പോർട്സ്";
            }
            else if(lang.equals("telugu")){
                strText = "క్రీడలు";
            }
            else if(lang.equals("kannada")){
                strText = "ಕ್ರೀಡೆ";
            }
            else if(lang.equals("bengali")){
                strText = "স্পোর্টস";
            }

            itemArrayList_trend.clear();
            for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                PostItem item = itemArrayList_trend_root.get(i);

                String txt = item.getText();
                if(txt.contains(strText)){
                    if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                        itemArrayList_trend.add(item);
                    }

                }

            }
        }
        else if(trendTabPos == 4){
            if(lang.equals("hindi")){
                strText = "राजनीति";
            }
            else if(lang.equals("tamil")){
                strText = "அரசியல்";
            }
            else if(lang.equals("malayalam")){
                strText = "രാഷ്ട്രീയം";
            }
            else if(lang.equals("telugu")){
                strText = "రాజకీయాలు";
            }
            else if(lang.equals("kannada")){
                strText = "ರಾಜಕೀಯ";
            }
            else if(lang.equals("bengali")){
                strText = "রাজনীতি";
            }
            itemArrayList_trend.clear();
            for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                PostItem item = itemArrayList_trend_root.get(i);

                String txt = item.getText();
                if(txt.contains(strText)){
                    if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                        itemArrayList_trend.add(item);
                    }

                }

            }
        }
        else if(trendTabPos == 5){
            if(lang.equals("hindi")){
                strText = "मोहब्बत";
            }
            else if(lang.equals("tamil")){
                strText = "காதல்";
            }
            else if(lang.equals("malayalam")){
                strText = "പ്രണയം";
            }
            else if(lang.equals("telugu")){
                strText = "ప్రేమ";
            }
            else if(lang.equals("kannada")){
                strText = "ಪ್ರೀತಿ";
            }
            else if(lang.equals("bengali")){
                strText = "ভালবাসা";
            }

            itemArrayList_trend.clear();
            for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                PostItem item = itemArrayList_trend_root.get(i);

                String txt = item.getText();
                if(txt.contains(strText)){
                    if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                        itemArrayList_trend.add(item);
                    }

                }

            }
        }
        else if(trendTabPos == 6){
            if(lang.equals("hindi")){
                strText = "कॉमेडी";
            }
            else if(lang.equals("tamil")){
                strText = "நகைச்சுவை";
            }
            else if(lang.equals("malayalam")){
                strText = "തമാശ";
            }
            else if(lang.equals("telugu")){
                strText = "కామెడీ";
            }
            else if(lang.equals("kannada")){
                strText = "ಕಾಮಿಡಿ";
            }
            else if(lang.equals("bengali")){
                strText = "বনাম";
            }

            itemArrayList_trend.clear();
            for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                PostItem item = itemArrayList_trend_root.get(i);

                String txt = item.getText();
                if(txt.contains(strText)){
                    if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                        itemArrayList_trend.add(item);
                    }

                }

            }
        }
        else if(trendTabPos == 7){
            if(lang.equals("hindi")){
                strText = "समाचार";
            }
            else if(lang.equals("tamil")){
                strText = "செய்திகள்";
            }
            else if(lang.equals("malayalam")){
                strText = "വാർത്ത";
            }
            else if(lang.equals("telugu")){
                strText = "న్యూస్";
            }
            else if(lang.equals("kannada")){
                strText = "ಸುದ್ದಿ";
            }
            else if(lang.equals("bengali")){
                strText = "খবর";
            }

            itemArrayList_trend.clear();
            for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                PostItem item = itemArrayList_trend_root.get(i);

                String txt = item.getText();
                if(txt.contains(strText)){
                    if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                        itemArrayList_trend.add(item);
                    }

                }

            }
        }

        tabLayout_trend.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                System.out.println("sis ooo" +tab.getPosition());

                int pos = tab.getPosition();

                trendTabPos = pos;

                if(pos == 0){
                    itemArrayList_trend.clear();
                    for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                        PostItem item = itemArrayList_trend_root.get(i);

                        if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            itemArrayList_trend.add(item);
                        }

                    }

                }
                else if(pos == 1){
                    if(lang.equals("hindi")){
                        strText = "सिनेमा";
                    }
                    else if(lang.equals("tamil")){
                        strText = "சினிமா";
                    }
                    else if(lang.equals("malayalam")){
                        strText = "സിനിമ";
                    }
                    else if(lang.equals("telugu")){
                        strText = "సినిమా";
                    }
                    else if(lang.equals("kannada")){
                        strText = "ಸಿನೆಮಾ";
                    }
                    else if(lang.equals("bengali")){
                        strText = "সিনেমা";
                    }

                    itemArrayList_trend.clear();
                    for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                        PostItem item = itemArrayList_trend_root.get(i);

                        String txt = item.getText();
                        if(txt.contains(strText)){
                            if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                                itemArrayList_trend.add(item);
                            }

                        }

                    }

                }
                else if(pos == 2){
                    if(lang.equals("hindi")){
                        strText = "खेल";
                    }
                    else if(lang.equals("tamil")){
                        strText = "விளையாட்டு";
                    }
                    else if(lang.equals("malayalam")){
                        strText = "ക്രിക്കറ്റ്";
                    }
                    else if(lang.equals("telugu")){
                        strText = "క్రికెట్";
                    }
                    else if(lang.equals("kannada")){
                        strText = "ಕ್ರಿಕೆಟ್";
                    }
                    else if(lang.equals("bengali")){
                        strText = "ক্রিকেট";
                    }

                    itemArrayList_trend.clear();
                    for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                        PostItem item = itemArrayList_trend_root.get(i);

                        String txt = item.getText();
                        if(txt.contains(strText)){
                            if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                                itemArrayList_trend.add(item);
                            }

                        }

                    }
                }
                else if(pos == 3){
                    if(lang.equals("hindi")){
                        strText = "क्रिकेट";
                    }
                    else if(lang.equals("tamil")){
                        strText = "கிரிக்கெட்";
                    }
                    else if(lang.equals("malayalam")){
                        strText = "സ്പോർട്സ്";
                    }
                    else if(lang.equals("telugu")){
                        strText = "క్రీడలు";
                    }
                    else if(lang.equals("kannada")){
                        strText = "ಕ್ರೀಡೆ";
                    }
                    else if(lang.equals("bengali")){
                        strText = "স্পোর্টস";
                    }

                    itemArrayList_trend.clear();
                    for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                        PostItem item = itemArrayList_trend_root.get(i);

                        String txt = item.getText();
                        if(txt.contains(strText)){
                            if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                                itemArrayList_trend.add(item);
                            }

                        }

                    }
                }
                else if(pos == 4){
                    if(lang.equals("hindi")){
                        strText = "राजनीति";
                    }
                    else if(lang.equals("tamil")){
                        strText = "அரசியல்";
                    }
                    else if(lang.equals("malayalam")){
                        strText = "രാഷ്ട്രീയം";
                    }
                    else if(lang.equals("telugu")){
                        strText = "రాజకీయాలు";
                    }
                    else if(lang.equals("kannada")){
                        strText = "ರಾಜಕೀಯ";
                    }
                    else if(lang.equals("bengali")){
                        strText = "রাজনীতি";
                    }
                    itemArrayList_trend.clear();
                    for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                        PostItem item = itemArrayList_trend_root.get(i);

                        String txt = item.getText();
                        if(txt.contains(strText)){
                            if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                                itemArrayList_trend.add(item);
                            }

                        }

                    }
                }
                else if(pos == 5){
                    if(lang.equals("hindi")){
                        strText = "मोहब्बत";
                    }
                    else if(lang.equals("tamil")){
                        strText = "காதல்";
                    }
                    else if(lang.equals("malayalam")){
                        strText = "പ്രണയം";
                    }
                    else if(lang.equals("telugu")){
                        strText = "ప్రేమ";
                    }
                    else if(lang.equals("kannada")){
                        strText = "ಪ್ರೀತಿ";
                    }
                    else if(lang.equals("bengali")){
                        strText = "ভালবাসা";
                    }

                    itemArrayList_trend.clear();
                    for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                        PostItem item = itemArrayList_trend_root.get(i);

                        String txt = item.getText();
                        if(txt.contains(strText)){
                            if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                                itemArrayList_trend.add(item);
                            }

                        }

                    }
                }
                else if(pos == 6){
                    if(lang.equals("hindi")){
                        strText = "कॉमेडी";
                    }
                    else if(lang.equals("tamil")){
                        strText = "நகைச்சுவை";
                    }
                    else if(lang.equals("malayalam")){
                        strText = "തമാശ";
                    }
                    else if(lang.equals("telugu")){
                        strText = "కామెడీ";
                    }
                    else if(lang.equals("kannada")){
                        strText = "ಕಾಮಿಡಿ";
                    }
                    else if(lang.equals("bengali")){
                        strText = "বনাম";
                    }

                    itemArrayList_trend.clear();
                    for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                        PostItem item = itemArrayList_trend_root.get(i);

                        String txt = item.getText();
                        if(txt.contains(strText)){
                            if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                                itemArrayList_trend.add(item);
                            }

                        }

                    }
                }
                else if(pos == 7){
                    if(lang.equals("hindi")){
                        strText = "समाचार";
                    }
                    else if(lang.equals("tamil")){
                        strText = "செய்திகள்";
                    }
                    else if(lang.equals("malayalam")){
                        strText = "വാർത്ത";
                    }
                    else if(lang.equals("telugu")){
                        strText = "న్యూస్";
                    }
                    else if(lang.equals("kannada")){
                        strText = "ಸುದ್ದಿ";
                    }
                    else if(lang.equals("bengali")){
                        strText = "খবর";
                    }

                    itemArrayList_trend.clear();
                    for(int i = 0 ; i < itemArrayList_trend_root.size();i++){
                        PostItem item = itemArrayList_trend_root.get(i);

                        String txt = item.getText();
                        if(txt.contains(strText)){
                            if(!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                                itemArrayList_trend.add(item);
                            }

                        }

                    }
                }

                populateCarList_trend();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        populateCarList_trend();
    }

    public void populateCarList_trend(){

        postItem2ArrayList_trend.clear();

        for(int i = 0; i < itemArrayList_trend.size(); i+=3){
            PostItem p1 = null;
            PostItem p2 = null;
            PostItem p3 = null;

            PostItem2 postItem2 = new PostItem2();

            if(i < itemArrayList_trend.size()){
                p1 = itemArrayList_trend.get(i);
                postItem2.setPostItem1(p1);
            }
            if(i+1 < itemArrayList_trend.size()){
                p2 = itemArrayList_trend.get(i+1);
                postItem2.setPostItem2(p2);
            }
            if(i+2 < itemArrayList_trend.size()){
                p3 = itemArrayList_trend.get(i+2);
                postItem2.setPostItem3(p3);
            }

            postItem2ArrayList_trend.add(postItem2);


        }


        adapter_trend = new TrendingAdapter2(getApplicationContext(),postItem2ArrayList_trend);
        adapter_trend.setOnCarItemClickListener(listener_trend);
        /*GridLayoutManager mng_layout = new GridLayoutManager(this.getActivity(), 3*//*In your case 4*//*);

        mng_layout.setSpanSizeLookup( new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return 1;
            }
        });

        recyclerView.setLayoutManager(mng_layout);*/
        recyclerView_trend.setAdapter(adapter_trend);


        //gridView.setAdapter(adapter);

    }

    TrendingAdapter2.OnCarItemClickListener listener_trend = new TrendingAdapter2.OnCarItemClickListener() {
        @Override
        public void onImg1Click(int position, PostItem item) {
            System.out.println("emmii   "+position);
            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                Intent intent = new Intent(getApplicationContext(),ImageDisplay.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("postId", item.getPostid());
                intent.putExtra("imgUrl", item.getPpurl());
                intent.putExtra("userName", item.getUsername());
                intent.putExtra("otherId", item.getUid());
                intent.putExtra("coverUrl", item.getImagelocation());
                intent.putExtra("likes", item.getLikes());
                intent.putExtra("comments", item.getCommentsno());
                intent.putExtra("shares", item.getShares());
                intent.putExtra("downloads", item.getDownloadsno());
                intent.putExtra("txt", item.getText());
                intent.putExtra("views", item.getViews());

                startActivity(intent);
            }else {
                postJSON_t = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList_trend.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList_trend.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    postJSON_t.put(object);


                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("items", postJSON_t.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);
            }

        }

        @Override
        public void onImg2Click(int position, PostItem item) {
            System.out.println("emmii   "+position+1);


            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                Intent intent = new Intent(getApplicationContext(),ImageDisplay.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("postId", item.getPostid());
                intent.putExtra("imgUrl", item.getPpurl());
                intent.putExtra("userName", item.getUsername());
                intent.putExtra("otherId", item.getUid());
                intent.putExtra("coverUrl", item.getImagelocation());
                intent.putExtra("likes", item.getLikes());
                intent.putExtra("comments", item.getCommentsno());
                intent.putExtra("shares", item.getShares());
                intent.putExtra("downloads", item.getDownloadsno());
                intent.putExtra("txt", item.getText());
                intent.putExtra("views", item.getViews());

                startActivity(intent);
            }else {
                postJSON_t = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList_trend.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList_trend.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    postJSON_t.put(object);


                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("items", postJSON_t.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);
            }
        }

        @Override
        public void onImg3Click(int position, PostItem item) {
            System.out.println("emmii   "+position+2);
            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                Intent intent = new Intent(getApplicationContext(),ImageDisplay.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("postId", item.getPostid());
                intent.putExtra("imgUrl", item.getPpurl());
                intent.putExtra("userName", item.getUsername());
                intent.putExtra("otherId", item.getUid());
                intent.putExtra("coverUrl", item.getImagelocation());
                intent.putExtra("likes", item.getLikes());
                intent.putExtra("comments", item.getCommentsno());
                intent.putExtra("shares", item.getShares());
                intent.putExtra("downloads", item.getDownloadsno());
                intent.putExtra("txt", item.getText());
                intent.putExtra("views", item.getViews());

                startActivity(intent);
            }else {
                postJSON_t = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList_trend.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList_trend.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    postJSON_t.put(object);


                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("items", postJSON_t.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);
            }

        }
    };

    public void feed(){


        useridd = preference.getString("uid","");
        lang = preference.getString("lang","");

        requestCameraPermission();

        mSwipeRefreshLayout = (SwipeRefreshLayout) feed.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                jsonParse();

            }
        });
    }

    private void foundDevice(View v){
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList=new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(v, "ScaleX", 0f, 1f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(v, "ScaleY", 0f, 1f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        v.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    private void viewMenu() {

        if (!isOpen) {

            if (ViewCompat.isAttachedToWindow(rlCreatePost))
            {
                System.out.println("only f    >>>>> anim   ");
            }
            int x = (int) imgCreatePost.getX()+100;
            int y = (int) rootView.getBottom()-100;

            int startRadius = 0;
            int endRadius = (int) Math.hypot(rlRootParent.getWidth(), rlRootParent.getHeight());

            //fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(),android.R.color.white,null)));
            //fab.setImageResource(R.drawable.ic_close_grey);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                rlCreatePost.setVisibility(View.VISIBLE);
                Animator anim = ViewAnimationUtils.createCircularReveal(rlCreatePost, x, y, startRadius, endRadius);

                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        rlRootParent.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

                anim.start();
            }
            else {
                rlCreatePost.setVisibility(View.VISIBLE);
                rlRootParent.setVisibility(View.GONE);

            }

            foundDevice(llc1);
            foundDevice(llc2);
            foundDevice(llc3);
            foundDevice(llc4);


            isOpen = true;

        } else {

            int x = (int) imgCreatePost.getX()+100;
            int y = rlCreatePost.getBottom()-100;

            int startRadius = Math.max(rlRootParent.getWidth(), rlRootParent.getHeight());
            int endRadius = 0;

            //fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(),R.color.colorAccent,null)));
            //fab.setImageResource(R.drawable.ic_plus_white);
            rlRootParent.setVisibility(View.VISIBLE);
            Animator anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(rlCreatePost, x, y, startRadius, endRadius);
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        rlCreatePost.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
                anim.start();
            }
            else {
                rlCreatePost.setVisibility(View.GONE);

            }

            llc1.setVisibility(View.INVISIBLE);
            llc2.setVisibility(View.INVISIBLE);
            llc3.setVisibility(View.INVISIBLE);
            llc4.setVisibility(View.INVISIBLE);


            isOpen = false;
        }
    }


    @Override
    public void onBackPressed() {

        if (isOpen){
            int x = (int) imgCreatePost.getX()+100;
            int y = rlCreatePost.getBottom()-100;

            int startRadius = Math.max(rlRootParent.getWidth(), rlRootParent.getHeight());
            int endRadius = 0;

            //fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(),R.color.colorAccent,null)));
            //fab.setImageResource(R.drawable.ic_plus_white);
            rlRootParent.setVisibility(View.VISIBLE);
            Animator anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(rlCreatePost, x, y, startRadius, endRadius);
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {

                        rlCreatePost.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
                anim.start();
            }
            else {
                rlCreatePost.setVisibility(View.GONE);

            }
            llc1.setVisibility(View.INVISIBLE);
            llc2.setVisibility(View.INVISIBLE);
            llc3.setVisibility(View.INVISIBLE);
            llc4.setVisibility(View.INVISIBLE);


            isOpen = false;
        }
        else {
            String sss = et_search.getText().toString();
            if(!sss.isEmpty()){
                rl1_search.setVisibility(View.VISIBLE);
                rl2_search.setVisibility(View.GONE);
                recyclerView_search.setVisibility(View.GONE);

                et_search.setText("");

            }
            else {
                if(isWhatsApp){


                    if (exoPlayer != null){
                        exoPlayer.setPlayWhenReady(false);
                        carsViewHolder_xl.play.setVisibility(View.VISIBLE);
                        carsViewHolder_xl.imgCover.setVisibility(View.VISIBLE);
                        carsViewHolder_xl.mExoPlayer.setVisibility(View.GONE);
                    }
                    rl_whatsApp_xl.setVisibility(View.GONE);
                    rlRootParent.setVisibility(View.VISIBLE);
                }else {
                    super.onBackPressed();
                }

            }


        }

    }

    public void requestCameraPermission() {
        System.out.println("only f >>>>>>>>>>>>>>>  feed");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            if(getApplicationContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    getApplicationContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSIONS_CODE);
            }
            else {
                if(itemArrayList.size() != 0){

                    for (int i = 0; i < itemArrayList.size();i++){
                        PostItem item = itemArrayList.get(i);
                        int count2 = db_sqliteFavorites.get_check_List_Favorite(item.getPostid());
                        if (count2 == 0) {
                            itemArrayList.get(i).setLiked(false);
                        } else {
                            itemArrayList.get(i).setLiked(true);
                        }

                        if (item.getUid().equals(useridd)){

                        }else {
                            int count3 = db_sqliteFollowing.get_check_List_Favorite(item.getUid());
                            if (count3 == 0) {
                                itemArrayList.get(i).setFollowing(false);
                            } else {
                                itemArrayList.get(i).setFollowing(true);
                            }
                        }

                    }


                    populateCarList();
                }
                else {
                    jsonParse();
                }

            }


        } else {
            if(itemArrayList.size() != 0){

                for (int i = 0; i < itemArrayList.size();i++){
                    PostItem item = itemArrayList.get(i);
                    int count2 = db_sqliteFavorites.get_check_List_Favorite(item.getPostid());
                    if (count2 == 0) {
                        itemArrayList.get(i).setLiked(false);
                    } else {
                        itemArrayList.get(i).setLiked(true);
                    }

                    if (item.getUid().equals(useridd)){

                    }else {
                        int count3 = db_sqliteFollowing.get_check_List_Favorite(item.getUid());
                        if (count3 == 0) {
                            itemArrayList.get(i).setFollowing(false);
                        } else {
                            itemArrayList.get(i).setFollowing(true);
                        }
                    }

                }


                populateCarList();
            }
            else {
                jsonParse();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int reqCode, String[] perms, int[] results) {
        if (reqCode == REQUEST_STORAGE_PERMISSIONS_CODE) {
            if (results.length > 0 && results[0] == PackageManager.PERMISSION_GRANTED && results[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED ) {


                    System.out.println("here");

                    jsonParse();
                }
                else {
                    requestCameraPermission();
                }

            }else {
                requestCameraPermission();
            }
        }
    }

    public void populateCarList(){
        progressBar.setVisibility(View.GONE);
        if(itemArrayList2.size() == 0){
            counter = 0;
            for(int i = counter ; i < counter+10; i++){
                if(i == itemArrayList.size()){
                    break;
                }
                itemArrayList.get(i).setPrepared(true);
                itemArrayList2.add(itemArrayList.get(i));
            }

        }
        counter=counter+10;

        //Collections.shuffle(itemArrayList2);

        adapter = new ExoAdapter(itemArrayList2,getApplicationContext());
        adapter.setOnCarItemClickListener(listener);

        recyclerView.setAdapter(adapter);

        mLayoutManager.scrollToPosition(itemPosition);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(final RecyclerView recyclerView, int scrollState) {

                if(!recyclerView.canScrollVertically(1)){

                    progressBar.setVisibility(View.VISIBLE);
                    final Handler handler=new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            progressBar.setVisibility(View.GONE);


                            if(counter <= itemArrayList.size()){
                                for(int i = counter ; i < counter+10; i++){
                                    if(i == itemArrayList.size()){
                                        System.out.println("only size"+itemArrayList.size());
                                        break;
                                    }
                                    else {
                                        itemArrayList2.add(itemArrayList.get(i));
                                    }

                                }

                                counter=counter+10;
                                //Collections.shuffle(itemArrayList2);
                                adapter.setItemlist(itemArrayList2);
                                adapter.notifyDataSetChanged();
                            }


                        }
                    },500);
                }

                /*if(scrollState == RecyclerView.SCROLL_STATE_IDLE){

                    layoutmanager = (LinearLayoutManager)recyclerView.getLayoutManager();

                    int firstVisiblePosition = layoutmanager.findFirstCompletelyVisibleItemPosition();

                    int ddd = layoutmanager.findFirstVisibleItemPosition();
                    int eee = layoutmanager.findLastCompletelyVisibleItemPosition();
                    int fff = layoutmanager.findLastVisibleItemPosition();

                    //System.out.println("only f ======================================= "+firstVisiblePosition);
                    //System.out.println("only f 1st full visible "+firstVisiblePosition);
                    //System.out.println("only f last full visible "+eee);
                    //System.out.println("only f last partial visible "+fff);

                    if(ddd >= 0){
                        itemPosition = ddd;
                        System.out.println("only f 1st partial visible "+itemPosition);

                    }
                    else {
                        itemPosition=0;
                    }
                    if (firstVisiblePosition >= 0 || fff >= 0)
                    {

                        if (oldFocusedLayout != null)
                        {
                            System.out.println("only remove "+firstVisiblePosition);
                            if(exoPlayer != null){

                                carsViewHolder.play.setVisibility(View.VISIBLE);
                                carsViewHolder.imgCover.setVisibility(View.VISIBLE);
                                carsViewHolder.mExoPlayer.setVisibility(View.GONE);

                            }
                        }


                    }
                    if(exoPlayer != null){
                        exoPlayer.setPlayWhenReady(false);
                    }
                    currentFocusedLayout = layoutmanager.findViewByPosition(firstVisiblePosition);
                    //VideoView vv_dashboard = (VideoView)currentFocusedLayout.findViewById(R.id.vv_dashboard);
                    ////to play video of selected recylerview, videosData is an array-list which is send to recyclerview adapter to fill the view. Here we getting that specific video which is displayed through recyclerview.
                    //playVideo(videosData.get(positionView));
                    oldFocusedLayout = currentFocusedLayout;
                }*/
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });
    }

    boolean saveBitmapToFile(File dir, String fileName, Bitmap bm, Bitmap.CompressFormat format, int quality) {

        File imageFile = new File(dir,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);

            bm.compress(format,quality,fos);

            fos.close();

            return true;
        }
        catch (IOException e) {
            Log.e("app",e.getMessage());
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }

    ExoAdapter.OnCarItemClickListener listener = new ExoAdapter.OnCarItemClickListener() {
        @Override
        public void onllClicked(final int position, PostItem item, final ExoAdapter.CarsViewHolder holder) {


            System.out.println("only  >>>>>>>>> "+holder.imgCover.getHeight()+" "+holder.imgCover.getWidth());

            /*if (!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;



                holder.cardmore.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.play.setVisibility(View.GONE);

                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelection.Factory videoTrackSelectionFactory =
                        new AdaptiveTrackSelection.Factory(bandwidthMeter);
                TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

                if(exoPlayer != null){
                    exoPlayer.setPlayWhenReady(false);
                    exoPlayer.stop();
                }

                exoPlayer = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

                holder.mExoPlayer.setPlayer(exoPlayer);



                exoPlayer.addListener(new Player.EventListener() {
                    @Override
                    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                        System.out.println("only 1 "+position);
                    }

                    @Override
                    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                        System.out.println("only 2 "+position);
                    }

                    @Override
                    public void onLoadingChanged(boolean isLoading) {
                        System.out.println("only 3 "+position);
                    }

                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                        if (playWhenReady && playbackState == Player.STATE_READY) {
                            System.out.println("only  playing  "+position);
                            // media actually playing
                            holder.progressBar.setVisibility(View.GONE);
                            holder.play.setVisibility(View.GONE);
                            holder.imgCover.setVisibility(View.GONE);
                            holder.mExoPlayer.setVisibility(View.VISIBLE);
                            holder.mExoPlayer.getLayoutParams().height = holder.imgCover.getHeight();
                            holder.mExoPlayer.requestFocus();


                        } else if (playWhenReady) {
                            // might be idle (plays after prepare()),
                            // buffering (plays when data available)
                            // or ended (plays when seek away from end)
                            if (playbackState==Player.STATE_ENDED){
                                System.out.println("only  Ended  "+position);
                            }
                            else if (playbackState==Player.STATE_BUFFERING){
                                System.out.println("only Buffering "+position);
                            }
                            else if (playbackState==Player.STATE_IDLE){
                                System.out.println("only Idle "+position);
                            }
                        } else {
                            // player paused in any state
                            System.out.println("only  paused  "+position);



                            //horizontalCarList.get(position).setPlayWhenReady(true);

                        }

                    }

                    @Override
                    public void onRepeatModeChanged(int repeatMode) {

                    }

                    @Override
                    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                    }

                    @Override
                    public void onPlayerError(ExoPlaybackException error) {

                    }

                    @Override
                    public void onPositionDiscontinuity(int reason) {

                    }

                    @Override
                    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                    }

                    @Override
                    public void onSeekProcessed() {

                    }
                });


                CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

                MediaSource audioSource = new ExtractorMediaSource(Uri.parse(itemArrayList2.get(position).getVideoUrl()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


                exoPlayer.seekTo(position, 0);

                exoPlayer.prepare(audioSource, true, false);

                System.out.println("only  here  "+itemArrayList2.get(position).getPrepared());

                exoPlayer.setPlayWhenReady(true);


                adapterClickedPos=position;
                carsViewHolder = holder;

            }else {
                holder.play.setVisibility(View.GONE);

            }*/



            //System.out.println("only ??????????????????????????????????????? select "+position+itemArrayList2.get(position).getPlayWhenReady());
            //adapter.setItemlist(itemArrayList2);
            //adapter.notifyDataSetChanged();

            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                holder.play.setVisibility(View.GONE);
            }else {


                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);

                itemPosition=position;

                float views = 0;
                String vvv="";
                views = itemArrayList.get(position).getViews();
                views=views+1;
                itemArrayList.get(position).setViews((int)views);

                if(views<=999){
                    int vvvvv = (int)views;
                    vvv = String.valueOf(vvvvv)+" views";
                }
                else if(views >= 1000){
                    views = views/1000;
                    vvv = String.valueOf(views)+"k views";
                }else if(views >= 1000000){
                    views = views/1000000;
                    vvv = String.valueOf(views)+"M views";
                }

                holder.tvViews.setText(vvv);


            }
        }

        @Override
        public void onCoverClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            System.out.println("only ??????????????????????????????????????? select "+position);


            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                Intent intent = new Intent(getApplicationContext(),ImageDisplay.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("postId", item.getPostid());
                intent.putExtra("imgUrl", item.getPpurl());
                intent.putExtra("userName", item.getUsername());
                intent.putExtra("otherId", item.getUid());
                intent.putExtra("coverUrl", item.getImagelocation());
                intent.putExtra("likes", item.getLikes());
                intent.putExtra("comments", item.getCommentsno());
                intent.putExtra("shares", item.getShares());
                intent.putExtra("downloads", item.getDownloadsno());
                intent.putExtra("txt", item.getText());
                intent.putExtra("views", item.getViews());

                startActivity(intent);
            }else {
                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);
            }

            itemPosition=position;
            float views = 0;
            String vvv="";
            views = itemArrayList.get(position).getViews();
            views=views+1;
            itemArrayList.get(position).setViews((int)views);

            if(views<=999){
                int vvvvv = (int)views;
                vvv = String.valueOf(vvvvv)+" views";
            }
            else if(views >= 1000){
                views = views/1000;
                vvv = String.valueOf(views)+"k views";
            }else if(views >= 1000000){
                views = views/1000000;
                vvv = String.valueOf(views)+"M views";
            }

            holder.tvViews.setText(vvv);


        }

        @Override
        public void onWhatsAppClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            if (!itemArrayList2.get(position).getTime().contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(position).getText()+"\n"+whatsAppText);
                }
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Title", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }


                int shar = itemArrayList2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                itemArrayList2.get(position).setShares(shar);
                itemArrayList.get(position).setShares(shar);
                adapter.setItemlist(itemArrayList2);


                StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                            }
                        }

                        , new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("postid", itemArrayList2.get(position).getPostid());

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringrequest);

            }else {


                int shar = itemArrayList2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }


                downloadvideoWhatsApp downloadvideo2=new downloadvideoWhatsApp();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList2.get(position).getVideoUrl(),time2,String.valueOf(position));

            }
        }

        @Override
        public void onLikeClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }else {
                viewHolder.imgLike.setSelected(!viewHolder.imgLike.isSelected());
                int count = db_sqliteFavorites.get_check_List_Favorite(itemArrayList2.get(position).getPostid());
                if (count == 0) {
                    viewHolder.imgLike.setImageResource(R.drawable.love2);
                    int lik = itemArrayList2.get(position).getLikes();

                    itemArrayList2.get(position).setLikes(lik+1);
                    itemArrayList.get(position).setLikes(lik+1);
                    adapter.setItemlist(itemArrayList2);

                    itemArrayList.get(position).setLiked(true);
                    itemArrayList2.get(position).setLiked(true);

                    adapter.setItemlist3(true,position);

                    db_sqliteFavorites.Inserttoalertlist(itemArrayList2.get(position).getPostid());

                    viewHolder.tvLikes.setText(String.valueOf(lik + 1));

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/sociallikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);
                } else {
                    viewHolder.imgLike.setImageResource(R.drawable.dislike);
                    db_sqliteFavorites.Delete(itemArrayList2.get(position).getPostid());

                    int lik = itemArrayList2.get(position).getLikes();

                    itemArrayList2.get(position).setLikes(lik-1);
                    itemArrayList.get(position).setLikes(lik-1);
                    adapter.setItemlist(itemArrayList2);

                    itemArrayList.get(position).setLiked(false);
                    itemArrayList2.get(position).setLiked(false);

                    adapter.setItemlist3(false,position);

                    if (lik == 0) {
                        viewHolder.tvLikes.setText("Like");
                    }

                    viewHolder.tvLikes.setText(String.valueOf(lik - 1));

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialdislikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
            }
        }

        @Override
        public void onCommentsClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                itemForComment = position;
                carsViewHolder_comments = holder;
                Intent intent=new Intent(getApplicationContext(),Comments.class);
                intent.putExtra("postid",itemArrayList2.get(position).getPostid());
                startActivityForResult(intent,11);
            }
        }

        @Override
        public void onDownloadsClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            viewHolder.progressBar_down.setVisibility(View.VISIBLE);

            if (!itemArrayList2.get(position).getTime().contains("/")){


                try{
                    Bitmap bm = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();

                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + " Flash/");

                    boolean doSave = true;
                    if (!f.exists()) {
                        doSave = f.mkdirs();
                    }

                    if (doSave) {
                        saveBitmapToFile(f,itemArrayList2.get(position).getPostid()+".png",bm,Bitmap.CompressFormat.PNG,100);
                    }
                    else {
                        Log.e("app","Couldn't create target directory.");
                    }


                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                    int down = itemArrayList2.get(position).getDownloadsno();
                    itemArrayList2.get(position).setDownloadsno(down+1);
                    itemArrayList.get(position).setDownloadsno(down+1);
                    viewHolder.tvDownloads.setText(String.valueOf(down+1));
                    adapter.setItemlist(itemArrayList2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {




                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("postid",itemArrayList2.get(position).postid);

                            return params;
                        }
                    };

                    RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                }catch (NullPointerException e){

                }




            }else {

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                int down = itemArrayList2.get(position).getDownloadsno();
                viewHolder.tvDownloads.setText(String.valueOf(down+1));

                downloadvideooo downloadvideo2=new downloadvideooo();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList2.get(position).getVideoUrl(),time2,String.valueOf(position));





            }





        }

        @Override
        public void onMoreClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {

            if(holder.cardmore.getVisibility() == View.VISIBLE){
                holder.cardmore.animate().alpha(0);
                holder.cardmore.setVisibility(View.GONE);
            }else {
                holder.cardmore.setVisibility(View.VISIBLE);
                holder.cardmore.animate().alpha(1);
            }

        }

        @Override
        public void onShareClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            if (!itemArrayList2.get(position).time.contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Image", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

            }else {

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                downloadvideoshare downloadvideo=new downloadvideoshare();
                downloadvideo.setdownloadlayout(viewHolder.ll_share);
                downloadvideo.setProgressbar(viewHolder.progressBar_share);
                downloadvideo.execute(itemArrayList2.get(position).getVideoUrl(), time2,String.valueOf(position));

            }

        }

        @Override
        public void onReportClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            Toast.makeText(getApplicationContext(), "Report Sent", Toast.LENGTH_SHORT).show();
            viewHolder.cardmore.animate().alpha(0);
            viewHolder.cardmore.setVisibility(View.GONE);
        }

        @Override
        public void onAddClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();

            FirebaseUser mUser = mAuth.getCurrentUser();

            if(mUser == null){
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                if (viewHolder.tvAdd.getText().toString().equals("Follow")) {
                    db_sqliteFollowing.Inserttoalertlist(itemArrayList2.get(position).getUid());
                    viewHolder.tvAdd.setText("Unfollow");

                    adapter.setItemlist2(itemArrayList2.get(position).getUid(),true);

                    follow();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/followings.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList2.get(position).getUid());


                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }else {
                    db_sqliteFollowing.Delete(itemArrayList2.get(position).uid);
                    viewHolder.tvAdd.setText("Follow");

                    adapter.setItemlist2(itemArrayList2.get(position).getUid(),false);

                    follow();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/unfollow.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList2.get(position).getUid());


                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }


            }
        }

        @Override
        public void onProPicClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {

            if(useridd.equals(item.getUid())){
                Intent intent = new Intent(getApplicationContext(),Profile.class);
                startActivity(intent);

            }else {
                Intent intent = new Intent(getApplicationContext(),OtherUserProfile.class);
                intent.putExtra("uid",item.getUid());
                intent.putExtra("userName",item.getUsername());
                intent.putExtra("imgUrl",item.getPpurl());
                startActivity(intent);
            }
        }
    };










    public void jsonParse(){
        String url = "http://heyapp.in/showposts.php";

        progressBar_trend.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressBar_follow.setVisibility(View.VISIBLE);
        System.out.println("rrrrrrrrrrrrrrrrrrr**************");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response


                        itemArrayList.clear();
                        itemArrayList2.clear();
                        itemArrayList_follow.clear();
                        itemArrayList_follow2.clear();

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("eventss");

                            map.clear();
                            for(int i = 0 ; i < array.length();i++) {
                                JSONObject object = array.getJSONObject(i);

                                /*object.getString("language").equals("bengali")||
                                        object.getString("language").equals("hindi")||
                                        object.getString("language").equals("tamil")*/
                                //language
                                if(!object.getString("uid").isEmpty() && !object.getString("language").isEmpty()){
                                    if(lang.equals(object.getString("language"))){
                                        PostItem item = new PostItem(getApplicationContext());
                                        item.setUid(object.getString("uid"));
                                        item.setPostid(object.getString("postid"));
                                        item.setUsername(object.getString("username"));
                                        item.setTime(object.getString("time"));
                                        item.setImagelocation("https://static.heyapp.in/file/app-media/"+object.getString("imagelocation"));

                                        item.setText(object.getString("text"));
                                        item.setDownloadsno(object.getInt("downloads"));
                                        item.setLikes(object.getInt("likes"));
                                        item.setShares(object.getInt("shares"));
                                        item.setCommentsno(object.getInt("comments"));
                                        item.setViews(object.getInt("views"));



                                        String[] splitter =item.getTime().split("/");
                                        String time1 = splitter[0];
                                        String time2="";
                                        String time = "";

                                        // https:\/\/static.heyapp.in\/file\/app-media\/MArzuAxcATgdP3IuQkr2f248hL7220181114142546.jpg
                                        // https://static.heyapp.in/file/app-media/MArzuAxcATgdP3IuQkr2f248hL7220181114142546.jpg
                                        try {
                                            time2 = splitter[1];
                                            //time = time1.substring(0, time1.length() - 1);
                                        }catch (ArrayIndexOutOfBoundsException e){

                                        }

                                        if(item.getTime().contains("/")){
                                            time = time1.substring(0, time1.length() - 1);
                                            System.out.println("only  -------     " + object.getString("username")+""+time);

                                        }else {
                                            time = item.getTime();
                                        }
                                        System.out.println("only ff  ------->>>>>>>>     " + object.getString("username")+"   "+object.getString("language"));

                                        item.setVideoUrl("https://static.heyapp.in/file/app-media/"+time2);

                                        String temp = object.getString("ppurl");


                                        item.setPpurl(temp);

                                        int count2 = db_sqliteFavorites.get_check_List_Favorite(item.getPostid());
                                        if (count2 == 0) {
                                            item.setLiked(false);
                                        } else {
                                            item.setLiked(true);
                                        }

                                        if (item.getUid().equals(useridd)){

                                        }else {
                                            int count3 = db_sqliteFollowing.get_check_List_Favorite(item.getUid());
                                            if (count3 == 0) {
                                                item.setFollowing(false);
                                            } else {
                                                item.setFollowing(true);
                                            }
                                        }

                                        if(!item.getText().isEmpty()){
                                            String[] www = item.getText().split("==");// i converted string to String[] array


                                            String text = www[0];


                                            //System.out.println("only >> hash  Text          ^^^^^^^^^^^^^^    "+text);

                                            try{
                                                String hashes = www[1];
                                                System.out.println("only >> hash  ^^^^^^^^^^^^^^    "+hashes);
                                                if(!hashes.isEmpty()){
                                                    List<String> tt=new ArrayList<String>();

                                                    String[] words = hashes.split("#");// i converted string to String[] array

                                                    System.out.println(words.length);
                                                    String fullText="";
                                                    for(int i1=0;i1<words.length;i1++){

                                                        if(!words[i1].isEmpty()){
                                                            tt.add(words[i1]);
                                                        }

                                                    }

                                                   item.setWord_list(tt);

                                                }
                                            }catch (ArrayIndexOutOfBoundsException e){
                                                //carsViewHolder.hashText.setVisibility(View.GONE);
                                                String hashes = text;
                                                //System.out.println("only >> hash  ^^^^^^^^^^^^^^    "+hashes);
                                                if(!hashes.isEmpty()){
                                                    List<String> word_list=new ArrayList<String>();

                                                    String[] words = hashes.split("#");// i converted string to String[] array

                                                    System.out.println(words.length);
                                                    String fullText="";
                                                    for(int i1=0;i1<words.length;i1++){
                                                        if(words[i1].startsWith("#")){

                                                            if(!words[i1].isEmpty()){
                                                                word_list.add(words[i1]);
                                                            }

                                                            String next = "<font color='#FC4C21'>"+words[i1]+"</font>";
                                                            fullText = fullText + next;
                                                        }
                                                        else {
                                                            fullText = fullText+words[i1];
                                                            if(!words[i1].isEmpty()){
                                                                word_list.add(words[i1]);
                                                            }

                                                        }

                                                    }


                                                    item.setWord_list(word_list);

                                                }
                                            }


                                            item.setText(text);


                                        }

                                        map.put(time,item);
                                    }
                                }



                            }

                            Set keys = map.keySet();
                            final Iterator it = keys.iterator();

                            System.out.println("only All keys are: " + keys);

                            //ArrayList<PostItem> aaaa = new ArrayList<>();

                            postJSON = new JSONArray();

                            itemArrayList.clear();
                            itemArrayList2.clear();

                            while(it.hasNext()){
                                Object key = it.next();

                                System.out.println("only "+key + ": " + map.get(key).getUsername());

                                PostItem item = new PostItem(getApplicationContext());

                                item.setUid(map.get(key).getUid());

                                item.setPostid(map.get(key).getPostid());

                                item.setUsername(map.get(key).getUsername());

                                item.setTime(map.get(key).getTime());

                                item.setImagelocation(map.get(key).getImagelocation());

                                item.setVideoUrl(map.get(key).getVideoUrl());

                                item.setPpurl(map.get(key).getPpurl());

                                item.setText(map.get(key).getText());

                                item.setCommentsno(map.get(key).getCommentsno());

                                item.setDownloadsno(map.get(key).getDownloadsno());

                                item.setLikes(map.get(key).getLikes());

                                item.setShares(map.get(key).getShares());

                                item.setPlaying(false);

                                item.setFollowing(map.get(key).isFollowing());

                                item.setLiked(map.get(key).isLiked());

                                item.setWord_list(map.get(key).getWord_list());

                                item.setViews(map.get(key).getViews());

                                itemArrayList.add(item);
                                //aaaa.add(item);


                            }
                            Collections.shuffle(itemArrayList);

                            ArrayList<PostItem> aaaa = new ArrayList<>();

                            aaaa = itemArrayList;

                            for(int i = 0;i < itemArrayList.size() ;i++) {
                                //itemArrayList.add(aaaa.get(i));
                                System.out.println("only >>>>>>>>>>>> views   "+itemArrayList.get(i).getViews());
                                JSONObject object = new JSONObject();
                                PostItem item = new PostItem(getApplicationContext());

                                item.setUsername(aaaa.get(i).getUsername());
                                object.put("userName",item.getUsername());

                                item.setUid(aaaa.get(i).getUid());
                                object.put("uid",item.getUid());

                                item.setPostid(aaaa.get(i).getPostid());
                                object.put("postid",item.getPostid());

                                item.setTime(aaaa.get(i).getTime());
                                object.put("time",item.getTime());

                                item.setImagelocation(aaaa.get(i).getImagelocation());
                                object.put("imgUrl",item.getImagelocation());

                                item.setVideoUrl(aaaa.get(i).getVideoUrl());
                                object.put("videoUrl",item.getVideoUrl());

                                item.setPpurl(aaaa.get(i).getPpurl());
                                object.put("ppurl",item.getPpurl());

                                item.setText(aaaa.get(i).getText());
                                object.put("text",item.getText());

                                item.setCommentsno(aaaa.get(i).getCommentsno());
                                object.put("comments",item.getCommentsno());

                                item.setDownloadsno(aaaa.get(i).getDownloadsno());
                                object.put("downloads",item.getDownloadsno());

                                item.setLikes(aaaa.get(i).getLikes());
                                object.put("likes",item.getLikes());

                                item.setShares(aaaa.get(i).getShares());
                                object.put("shares",item.getShares());

                                item.setFollowing(aaaa.get(i).isFollowing());
                                object.put("isFollowing",item.isFollowing());

                                item.setLiked(aaaa.get(i).isLiked());
                                object.put("isLiked",item.isLiked());


                                item.setViews(aaaa.get(i).getViews());
                                object.put("views",item.getViews());

                                postJSON.put(object);

                            }

                            progressBar_trend.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            progressBar_follow.setVisibility(View.GONE);
                            mSwipeRefreshLayout.setRefreshing(false);

                            System.out.println("only >>>>>>>>>>>> size"+itemArrayList.size());
                            //getImageBitmap();
                            populateCarList();

                            follow();
                            trend();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+error.toString());
                    }
                }
        ) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", ACCESS_TOKEN);
                return params;
            }*/

            /*@Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", phone);
                params.put("otp", otp);
                params.put("timestamp", timeStamp);
                params.put("fcm_token", fcm_token);


                return params;
            }*/
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                //statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }







    class downloadvideooo extends AsyncTask<String,Integer,Void> {
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        String videoname;
        int pos;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Void doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                pos=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            circleProgressBar.setVisibility(View.INVISIBLE);
            linearLayout.setVisibility(View.VISIBLE);





            int down = itemArrayList2.get(pos).getDownloadsno();
            itemArrayList2.get(pos).setDownloadsno(down+1);
            itemArrayList.get(pos).setDownloadsno(down+1);
            adapter.setItemlist(itemArrayList2);

            StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {




                        }
                    }

                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();

                    params.put("postid",itemArrayList2.get(pos).postid);

                    return params;
                }
            };

            RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringrequest);


//            ddd ddd=new ddd();
//            ddd.setdownloadlayout(linearLayout);
//            ddd.setProgressbar(circleProgressBar);
//            ddd.execute(videoname);


        }
    }

    class downloadvideoWhatsApp extends AsyncTask<String,Integer,Boolean> {

        int positionn;
        String videoname;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
            progresss=0;
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                positionn=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){


                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);

                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");

                if (!itemArrayList2.get(positionn).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(positionn).getText()+"\n"+whatsAppText);
                }
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                    int share = itemArrayList2.get(positionn).getShares();
                    itemArrayList2.get(positionn).setShares(share+1);
                    itemArrayList.get(positionn).setShares(share+1);
                    adapter.setItemlist(itemArrayList2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(positionn).getPostid());

                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }






                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.linearLayout.setVisibility(View.VISIBLE);

            }






        }


    }

    class downloadvideoshare extends AsyncTask<String,Integer,Boolean> {

        String videoname;
        int position;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                position=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");

                urlConnection.setRequestProperty("Accept-Encoding","identity");
                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;

                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);
                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){



                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);
                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                if (!itemArrayList2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(position).getText()+"\n"+whatsAppText);
                }
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);
            }






        }


    }



    class downloadvideoooFollow extends AsyncTask<String,Integer,Void> {
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        String videoname;
        int pos;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Void doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                pos=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            circleProgressBar.setVisibility(View.INVISIBLE);
            linearLayout.setVisibility(View.VISIBLE);





            int down = itemArrayList_follow2.get(pos).getDownloadsno();
            itemArrayList_follow2.get(pos).setDownloadsno(down+1);
            itemArrayList_follow.get(pos).setDownloadsno(down+1);
            adapter.setItemlist(itemArrayList2);

            StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {




                        }
                    }

                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();

                    params.put("postid",itemArrayList_follow2.get(pos).postid);

                    return params;
                }
            };

            RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringrequest);


//            ddd ddd=new ddd();
//            ddd.setdownloadlayout(linearLayout);
//            ddd.setProgressbar(circleProgressBar);
//            ddd.execute(videoname);


        }
    }

    class downloadvideoWhatsAppFollow extends AsyncTask<String,Integer,Boolean> {

        int positionn;
        String videoname;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
            progresss=0;
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                positionn=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/SocialTest").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){


                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);

                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");

                if (!itemArrayList_follow2.get(positionn).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList_follow2.get(positionn).getText()+"\n"+whatsAppText);
                }
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                    int share = itemArrayList_follow2.get(positionn).getShares();
                    itemArrayList_follow2.get(positionn).setShares(share+1);
                    itemArrayList_follow.get(positionn).setShares(share+1);
                    adapter.setItemlist(itemArrayList_follow2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList_follow2.get(positionn).getPostid());

                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }






                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.linearLayout.setVisibility(View.VISIBLE);

            }






        }


    }

    class downloadvideoshareFollow extends AsyncTask<String,Integer,Boolean> {

        String videoname;
        int position;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                position=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");

                urlConnection.setRequestProperty("Accept-Encoding","identity");
                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;

                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);
                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){



                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);
                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                if (!itemArrayList_follow2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList_follow2.get(position).getText()+"\n"+whatsAppText);
                }
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);
            }






        }


    }



    class downloadvideoooSearch extends AsyncTask<String,Integer,Void> {
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        String videoname;
        int pos;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Void doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                pos=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            circleProgressBar.setVisibility(View.INVISIBLE);
            linearLayout.setVisibility(View.VISIBLE);





            int down = itemArrayList_search.get(pos).getDownloadsno();
            itemArrayList_search.get(pos).setDownloadsno(down+1);
            adapter.setItemlist(itemArrayList_search);

            StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {




                        }
                    }

                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();

                    params.put("postid",itemArrayList_search.get(pos).postid);

                    return params;
                }
            };

            RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringrequest);


//            ddd ddd=new ddd();
//            ddd.setdownloadlayout(linearLayout);
//            ddd.setProgressbar(circleProgressBar);
//            ddd.execute(videoname);


        }
    }

    class downloadvideoWhatsAppSearch extends AsyncTask<String,Integer,Boolean> {

        int positionn;
        String videoname;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
            progresss=0;
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                positionn=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){


                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);

                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");

                if (!itemArrayList_search.get(positionn).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList_search.get(positionn).getText()+"\n"+whatsAppText);
                }
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                    int share = itemArrayList_search.get(positionn).getShares();
                    itemArrayList_search.get(positionn).setShares(share+1);
                    adapter.setItemlist(itemArrayList_search);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList_search.get(positionn).getPostid());

                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }






                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.linearLayout.setVisibility(View.VISIBLE);

            }






        }


    }

    class downloadvideoshareSearch extends AsyncTask<String,Integer,Boolean> {

        String videoname;
        int position;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                position=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");

                urlConnection.setRequestProperty("Accept-Encoding","identity");
                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;

                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);
                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){



                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);
                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                if (!itemArrayList_search.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList_search.get(position).getText()+"\n"+whatsAppText);
                }
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);
            }






        }


    }



    Uri selectedImageURI = null;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 11){

            int counter = data.getIntExtra("commentCounter",0);

            carsViewHolder_comments.tvComments.setText(String.valueOf(counter));
            System.out.println("only ff result back "+counter);
        }

        if(requestCode == SELECT_PHOTO){
            if(resultCode == RESULT_OK){
                selectedImageURI = data.getData();
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImageURI);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                byte[] byteArray = stream.toByteArray();

                System.out.println("only path >> "+selectedImageURI.getPath());

                Intent intent = new Intent(MainActivity.this,PostDisplay.class);
                intent.putExtra("Bitmap",byteArray);
                intent.putExtra("type","galleryPhoto");
                startActivity(intent);


            }
        }

        if(requestCode == SELECT_VIDEO){
            if(resultCode == RESULT_OK){
                video = data.getData();
                videoPath = getPath(video);


                Intent intent=new Intent(MainActivity.this,PostDisplay.class);
                intent.putExtra("uri",String.valueOf(videoPath));
                intent.putExtra("type","video");
                startActivity(intent);



                /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                    Uri uri = data.getData();

                    final String[] split = uri.getPath().toString().split(":");

                    String filePath = split[1];

                    System.out.println("only path >> 1"+filePath);
                    Intent intent = new Intent(MainActivity.this,PostDisplay.class);
                    intent.putExtra("type","video");
                    intent.putExtra("uri",filePath);

                    //System.out.println("only path >> 3"+uri.toString());
                    startActivity(intent);


                }
                else {
                    Uri uri_video = data.getData();
                    try {
                        String filePath=PathUtil.getPath(getApplicationContext(),uri_video);
                        Intent intent = new Intent(MainActivity.this,PostDisplay.class);
                        intent.putExtra("type","video");
                        intent.putExtra("uri",filePath);

                        startActivity(intent);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                }*/

            }
        }
    }

    void writeFile(InputStream in, File file) {
        OutputStream out = null;
        try {
            out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if ( out != null ) {
                    out.close();
                }
                in.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }


    private String getPath(Uri uri) {

        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);


        } else
            return null;
    }


    class BackgroundTask extends AsyncTask<Void,Void,String>
    {

        String json_url;

        @Override
        protected void onPreExecute() {



        }


        @Override
        protected String doInBackground(Void... params) {


            file= new File(Environment.getExternalStorageDirectory()+WHATSAPP_STATUSES_LOCATION);

            Date currentdate=new Date();
            SimpleDateFormat dateFormat=new SimpleDateFormat("MMM");
            SimpleDateFormat dayformat=new SimpleDateFormat("dd");
            String currentmonth=dateFormat.format(currentdate);
            String currentday=dayformat.format(currentdate);

            showmore="no";

            if (file.isDirectory())
            {
                listFile = file.listFiles();

                if (listFile!=null){

                    while (countoffset<listFile.length)
                    {
                        Date date=new Date(listFile[countoffset].lastModified());
                        SimpleDateFormat dateFormat2=new SimpleDateFormat("MMddHHmm");
                        SimpleDateFormat dayformat2=new SimpleDateFormat("dd");

                        int currentmonth2=Integer.parseInt(dateFormat2.format(date));


                        showmore="yes";

                        statusexists="yes";



                        if (listFile[countoffset].getName().contains(".mp4")||listFile[countoffset].getName().contains(".MP4")
                                ||listFile[countoffset].getName().contains(".3gp")||listFile[countoffset].getName().contains(".3GP")||
                                listFile[countoffset].getName().contains(".jpg")||listFile[countoffset].getName().contains(".png")||listFile[countoffset].getName().contains(".jpeg"))
                        {
                            newarray.add(new listitemsnew(listFile[countoffset].getAbsolutePath(),currentmonth2));
                            System.out.println("only what >>>> "+listFile[countoffset].getName());

                            WhatsAppItem item = new WhatsAppItem();
                            item.setFilePath(listFile[countoffset].getAbsolutePath());

                            if(listFile[countoffset].getName().contains(".mp4")||listFile[countoffset].getName().contains(".MP4")
                                    ||listFile[countoffset].getName().contains(".3gp")||listFile[countoffset].getName().contains(".3GP")){
                                item.setFileType("video");
                                Bitmap bitmap= ThumbnailUtils.createVideoThumbnail(item.getFilePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                                if (bitmap!=null){
                                   item.setBitmap(bitmap);
                                }
                            }
                            else if(listFile[countoffset].getName().contains(".jpg")||listFile[countoffset].getName().contains(".png")||listFile[countoffset].getName().contains(".jpeg")){
                                item.setFileType("image");
                                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                                Bitmap bitmap = BitmapFactory.decodeFile(item.getFilePath(),bmOptions);
                                item.setBitmap(bitmap);
                            }

                            whatsAppItemArrayList.add(item);
                        }


                        countoffset++;

                    }


                    //Collections.sort(newarray, sortNameAtoZ);
                }


            }


            return null;
        }



        @Override
        protected void onProgressUpdate(Void...values) {
            super.onProgressUpdate();
        }






        @Override
        public void onPostExecute (String result) {

            if (statusexists.equals("no")){
                rl_whatsApp.setVisibility(View.GONE);

                //MainActivity.appBarLayout.setExpanded(false,true);
            }else{
                rl_whatsApp.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT
                );
                params.setMargins(0, dpToPx(100), 0, 0);
                mSwipeRefreshLayout.setLayoutParams(params);
                mSwipeRefreshLayout.requestFocus();
            }


            whatsAppAdapterSmall = new WhatsAppAdapterSmall(whatsAppItemArrayList,getApplicationContext());
            whatsAppAdapterSmall.setOnCarItemClickListener(listener_whatsApp);
            recyclerView_whatsApp.setAdapter(whatsAppAdapterSmall);

            whatsAppAdpaterLarge = new WhatsAppAdpaterLarge(whatsAppItemArrayList,getApplicationContext());
            whatsAppAdpaterLarge.setOnCarItemClickListener(listener_whatsApp_xl);
            recyclerView_whatsApp_xl.setAdapter(whatsAppAdpaterLarge);

            recyclerView_whatsApp_xl.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(final RecyclerView recyclerView, int scrollState) {

                    if (!recyclerView.canScrollVertically(1)) {


                    }

                    if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
                        View centerView = snapHelper_xl.findSnapView(mLayoutManager_xl);
                        final int position = mLayoutManager_xl.getPosition(centerView);

                        final WhatsAppAdpaterLarge.CarsViewHolder holder = (WhatsAppAdpaterLarge.CarsViewHolder) recyclerView.findViewHolderForAdapterPosition(position);

                        isPlaying = false;

                        if(whatsAppItemArrayList.get(position).getFileType().equals("video")){
                            holder.progressBar.setVisibility(View.VISIBLE);
                            holder.play.setVisibility(View.GONE);
                            holder.imgCover.setVisibility(View.VISIBLE);

                            isReady = false;

                            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                            TrackSelection.Factory videoTrackSelectionFactory =
                                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
                            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

                            if(exoPlayer != null){
                                exoPlayer.setPlayWhenReady(false);
                                exoPlayer.stop();
                            }

                            exoPlayer = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

                            holder.mExoPlayer.setPlayer(exoPlayer);



                            exoPlayer.addListener(new Player.EventListener() {
                                @Override
                                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                                    System.out.println("only 1 "+position);
                                }

                                @Override
                                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                                    System.out.println("only 2 "+position);
                                }

                                @Override
                                public void onLoadingChanged(boolean isLoading) {
                                    System.out.println("only 3 "+position);
                                }

                                @Override
                                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                                    if (playWhenReady && playbackState == Player.STATE_READY) {
                                        System.out.println("only  playing  "+position);
                                        // media actually playing
                                        holder.progressBar.setVisibility(View.GONE);
                                        holder.play.setVisibility(View.GONE);
                                        holder.imgCover.setVisibility(View.GONE);
                                        holder.mExoPlayer.setVisibility(View.VISIBLE);

                                        isPlaying = true;

                                        isReady = true;
                                    } else if (playWhenReady) {
                                        // might be idle (plays after prepare()),
                                        // buffering (plays when data available)
                                        // or ended (plays when seek away from end)
                                        if (playbackState==Player.STATE_ENDED){
                                            System.out.println("only  Ended  "+position);
                                        }
                                        else if (playbackState==Player.STATE_BUFFERING){
                                            System.out.println("only Buffering "+position);
                                        }
                                        else if (playbackState==Player.STATE_IDLE){
                                            System.out.println("only Idle "+position);
                                        }
                                    } else {
                                        // player paused in any state
                                        System.out.println("only  paused  "+position);



                                        //horizontalCarList.get(position).setPlayWhenReady(true);

                                    }

                                }

                                @Override
                                public void onRepeatModeChanged(int repeatMode) {

                                }

                                @Override
                                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                                }

                                @Override
                                public void onPlayerError(ExoPlaybackException error) {

                                }

                                @Override
                                public void onPositionDiscontinuity(int reason) {

                                }

                                @Override
                                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                                }

                                @Override
                                public void onSeekProcessed() {

                                }
                            });


                            CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

                            MediaSource audioSource = new ExtractorMediaSource(Uri.parse(whatsAppItemArrayList.get(position).getFilePath()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


                            exoPlayer.seekTo(position, 0);

                            exoPlayer.prepare(audioSource, true, false);

                            exoPlayer.setPlayWhenReady(true);
                            exoPlayer.getPlaybackState();
                            adapterClickedPos_xl=position;
                            carsViewHolder_xl = holder;

                        }else {
                            holder.progressBar.setVisibility(View.GONE);
                            holder.play.setVisibility(View.GONE);
                            holder.imgCover.setVisibility(View.VISIBLE);
                            if(exoPlayer != null){
                                exoPlayer.setPlayWhenReady(false);
                                exoPlayer.stop();
                            }
                        }

                    }


                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                }
            });



        }


    }

    WhatsAppAdapterSmall.OnCarItemClickListener listener_whatsApp = new WhatsAppAdapterSmall.OnCarItemClickListener() {
        @Override
        public void onWhatsAppClicked(int position, WhatsAppItem item, WhatsAppAdapterSmall.CarsViewHolder holder) {
            rl_whatsApp_xl.setVisibility(View.VISIBLE);
            rlRootParent.setVisibility(View.GONE);
            isWhatsApp = true;
            posOfItem_xl = position;
            mLayoutManager_xl.scrollToPosition(posOfItem_xl);
            final Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if(whatsAppItemArrayList.get(posOfItem_xl).getFileType().equals("video")){
                        View centerView = snapHelper_xl.findSnapView(mLayoutManager_xl);
                        final int position = mLayoutManager_xl.getPosition(centerView);

                        final WhatsAppAdpaterLarge.CarsViewHolder holder = (WhatsAppAdpaterLarge.CarsViewHolder) recyclerView_whatsApp_xl.findViewHolderForAdapterPosition(position);



                        holder.progressBar.setVisibility(View.VISIBLE);
                        holder.play.setVisibility(View.GONE);


                        isReady = false;

                        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                        TrackSelection.Factory videoTrackSelectionFactory =
                                new AdaptiveTrackSelection.Factory(bandwidthMeter);
                        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

                        if(exoPlayer != null){
                            exoPlayer.setPlayWhenReady(false);
                            exoPlayer.stop();
                        }

                        exoPlayer = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

                        holder.mExoPlayer.setPlayer(exoPlayer);



                        exoPlayer.addListener(new Player.EventListener() {
                            @Override
                            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                                System.out.println("only 1 "+posOfItem_xl);
                            }

                            @Override
                            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                                System.out.println("only 2 "+posOfItem_xl);
                            }

                            @Override
                            public void onLoadingChanged(boolean isLoading) {
                                System.out.println("only 3 "+posOfItem_xl);
                            }

                            @Override
                            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                                if (playWhenReady && playbackState == Player.STATE_READY) {
                                    System.out.println("only  playing  "+posOfItem_xl);
                                    // media actually playing
                                    holder.progressBar.setVisibility(View.GONE);
                                    holder.play.setVisibility(View.GONE);
                                    holder.imgCover.setVisibility(View.GONE);
                                    holder.mExoPlayer.setVisibility(View.VISIBLE);

                                    isPlaying = true;

                                    isReady = true;
                                } else if (playWhenReady) {
                                    // might be idle (plays after prepare()),
                                    // buffering (plays when data available)
                                    // or ended (plays when seek away from end)
                                    if (playbackState==Player.STATE_ENDED){
                                        System.out.println("only  Ended  "+posOfItem_xl);
                                    }
                                    else if (playbackState==Player.STATE_BUFFERING){
                                        System.out.println("only Buffering "+posOfItem_xl);
                                    }
                                    else if (playbackState==Player.STATE_IDLE){
                                        System.out.println("only Idle "+posOfItem_xl);
                                    }
                                } else {
                                    // player paused in any state
                                    System.out.println("only  paused  "+posOfItem_xl);



                                    //horizontalCarList.get(position).setPlayWhenReady(true);

                                }

                            }

                            @Override
                            public void onRepeatModeChanged(int repeatMode) {

                            }

                            @Override
                            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                            }

                            @Override
                            public void onPlayerError(ExoPlaybackException error) {

                            }

                            @Override
                            public void onPositionDiscontinuity(int reason) {

                            }

                            @Override
                            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                            }

                            @Override
                            public void onSeekProcessed() {

                            }
                        });


                        CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

                        MediaSource audioSource = new ExtractorMediaSource(Uri.parse(whatsAppItemArrayList.get(posOfItem_xl).getFilePath()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


                        exoPlayer.seekTo(posOfItem_xl, 0);

                        exoPlayer.prepare(audioSource, true, false);


                        exoPlayer.setPlayWhenReady(true);
                        exoPlayer.getPlaybackState();
                        adapterClickedPos_xl=posOfItem_xl;
                        carsViewHolder_xl = holder;

                    }else {

                        if(exoPlayer != null){
                            exoPlayer.setPlayWhenReady(false);
                            exoPlayer.stop();
                        }
                    }

                }
            },1000);
        }
    };



    public static Comparator<listitemsnew> sortNameAtoZ = new Comparator<listitemsnew>() {

        @Override
        public int compare(listitemsnew o1, listitemsnew o2) {

            Integer eventid1=o1.date;
            Integer eventid2=o2.date;
            return eventid2.compareTo(eventid1);

        }};




    public void populateHash(){
        hashListView.setAdapter(null);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        hashListView.setLayoutManager(linearLayoutManager);

        hashTagAdapter = new HashTagAdapter(hashList,getApplicationContext());
        hashTagAdapter.setOnCarItemClickListener(listenerHash);
        hashListView.setAdapter(hashTagAdapter);

    }

    HashTagAdapter.OnCarItemClickListener listenerHash = new HashTagAdapter.OnCarItemClickListener() {
        @Override
        public void onWhatsAppClicked(int position, String item, HashTagAdapter.CarsViewHolder holder) {
            search_text = item;
            et_search.setText(search_text);
            et_search.setSelection(et_search.getText().length());

        }
    };


    public void getHash(){
        hashList = new ArrayList<>();
        hashList2 = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, hashUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        hashList.clear();
                        hashList2.clear();

                        try {
                            JSONObject jsonObjecct = new JSONObject(response);
                            JSONArray jsonArraay = jsonObjecct.getJSONArray("eventss");

                            for(int i = 0 ; i < jsonArraay.length(); i++){
                                JSONObject obj = jsonArraay.getJSONObject(i);

                                hashList.add(obj.getString("hashtag1"));
                                hashList.add(obj.getString("hashtag2"));
                                hashList.add(obj.getString("hashtag3"));
                                hashList.add(obj.getString("hashtag4"));
                                hashList.add(obj.getString("hashtag5"));
                                hashList.add(obj.getString("hashtag6"));



                            }

                            populateHash();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Uid",useridd);
                params.put("Following", itemArrayList2.get(position).getUid());


                return params;
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


}
