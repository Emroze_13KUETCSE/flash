package com.status.fartech.socialmedia;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class PostPreDisplay extends AppCompatActivity {

    ImageView imgDel,imgOk,imgDisplay;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_pre_display);

        imgDel = findViewById(R.id.img_del);
        imgDisplay = findViewById(R.id.img_display);
        imgOk = findViewById(R.id.img_ok);

        Intent intent = getIntent();
        byte[] byteArray = intent.getByteArrayExtra("Bitmap");
        bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        imgDisplay.setImageBitmap(bitmap);

        imgOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        imgDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("action","del");
                setResult(99 , intent);
                finish();
            }
        });
    }
}
