package com.status.fartech.socialmedia;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VideoFullView extends AppCompatActivity {

    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;


    public ArrayList<PostItem> itemArrayList = new ArrayList<>();
    public ArrayList<PostItem> itemArrayList2 = new ArrayList<>();

    JSONArray postJson,postJSON;


    RecyclerView.LayoutManager mLayoutManager;
    int counter = 0;
    int REQUEST_STORAGE_PERMISSIONS_CODE = 1;
    private View currentFocusedLayout, oldFocusedLayout;
    private LinearLayoutManager layoutmanager;
    public SimpleExoPlayer exoPlayer;
    int adapterClickedPos = 0;
    ExoFullAdapter.CarsViewHolder carsViewHolder;

    ExoFullAdapter adapter;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    SnapHelper snapHelper;
    boolean isPlaying=false;

    boolean isReady = false;
    int posOfItem=0;

    int progresss,timeinsec;
    File loggo;

    DB_SqliteFollowing db_sqliteFollowing;
    String useridd;
    DB_SqliteFavorites db_sqliteFavorites;

    private int height;

    static int itemForComment = 0;
    ExoFullAdapter.CarsViewHolder carsViewHolder_comments;
    int originalPos=0;

    String lang="",whatsAppText="";

    boolean shouldPlay = true;
    Player.EventListener eee;

    boolean mustStop = false;

    boolean is1stEntered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_full_view);

        is1stEntered = true;

        Intent intent = getIntent();
        try {
            postJson = new JSONArray(intent.getStringExtra("items"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        posOfItem = intent.getIntExtra("pos",0);
        originalPos = intent.getIntExtra("original_pos",0);

        progressBar = findViewById(R.id.progress);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);

        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mLayoutManager.scrollToPosition(posOfItem);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setLayoutManager(mLayoutManager);

        adapter = new ExoFullAdapter(itemArrayList,getApplicationContext());
        adapter.setOnCarItemClickListener(listener);


        recyclerView.setAdapter(adapter);


        shouldPlay = true;
        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;

        db_sqliteFollowing = new DB_SqliteFollowing(getApplicationContext());
        db_sqliteFavorites = new DB_SqliteFavorites(getApplicationContext());




        lang = preference.getString("lang","");


        if(lang.equals("hindi")){

            whatsAppText = getString(R.string.w_hindi);
        }
        else if(lang.equals("tamil")){

            whatsAppText = getString(R.string.w_tamil);
        }
        else if(lang.equals("malayalam")){
            whatsAppText = getString(R.string.w_mala);
        }
        else if(lang.equals("telugu")){
            whatsAppText = getString(R.string.w_telegu);
        }
        else if(lang.equals("kannada")){
            whatsAppText = getString(R.string.w_kannada);
        }
        else if(lang.equals("bengali")){
            whatsAppText = getString(R.string.w_bangla);
        }



        //itemArrayList = getIntent().getParcelableArrayListExtra("items");

        for(int i = 0 ; i < postJson.length();i++){
            PostItem item = new PostItem(getApplicationContext());

            JSONObject object= null;
            try {
                object = postJson.getJSONObject(i);

                item.setUsername(object.getString("userName"));
                item.setUid(object.getString("uid"));
                item.setPostid(object.getString("postid"));
                item.setTime(object.getString("time"));
                item.setImagelocation(object.getString("imgUrl"));
                item.setVideoUrl(object.getString("videoUrl"));
                item.setText(object.getString("text"));
                item.setPpurl(object.getString("ppurl"));
                item.setDownloadsno(object.getInt("downloads"));
                item.setLikes(object.getInt("likes"));
                item.setShares(object.getInt("shares"));
                item.setCommentsno(object.getInt("comments"));
                item.setLiked(object.getBoolean("isLiked"));
                item.setFollowing(object.getBoolean("isFollowing"));
                item.setViews(object.getInt("views"));


                //System.out.println("only=here  "+itemArrayList.get(position).getPrepared());

                //exoPlayer.setPlayWhenReady(true);
                //exoPlayer.getPlaybackState();


                itemArrayList.add(item);
                itemArrayList2.add(item);


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        System.out.println("only   >>>>>>> size "+itemArrayList2.size());


        useridd = preference.getString("uid","");


        System.out.println("only >>>>>>>>>>>>>>>  feed");



        populateCarList();


    }

    boolean saveBitmapToFile(File dir, String fileName, Bitmap bm,
                             Bitmap.CompressFormat format, int quality) {

        File imageFile = new File(dir,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);

            bm.compress(format,quality,fos);

            fos.close();

            return true;
        }
        catch (IOException e) {
            Log.e("app",e.getMessage());
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public void onPause() {

        super.onPause();


        //Toast.makeText(this, "onPause 1", Toast.LENGTH_SHORT).show();
        shouldPlay=false;
        if (exoPlayer != null){
            exoPlayer.setPlayWhenReady(false);
            exoPlayer.getPlaybackState();

            carsViewHolder.play.setVisibility(View.VISIBLE);
            carsViewHolder.imgCover.setVisibility(View.VISIBLE);
            carsViewHolder.mExoPlayer.setVisibility(View.GONE);
            //Toast.makeText(this, "onPause 2", Toast.LENGTH_SHORT).show();
            mustStop=true;
            System.out.println("only= >>>>>>>>> onPaused  ");

        }


    }


    @Override
    protected void onDestroy() {
        //Toast.makeText(this, "onDestroy 1", Toast.LENGTH_SHORT).show();
        if (exoPlayer != null){
            exoPlayer.stop();
            exoPlayer.release();
            carsViewHolder.play.setVisibility(View.VISIBLE);
            carsViewHolder.imgCover.setVisibility(View.VISIBLE);
            carsViewHolder.mExoPlayer.setVisibility(View.GONE);
            //Toast.makeText(this, "onDestroy 2", Toast.LENGTH_SHORT).show();
        }
        shouldPlay=false;
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        is1stEntered = true;

        shouldPlay=true;
    }


    public void populateCarList(){

        adapter.notifyDataSetChanged();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(final RecyclerView recyclerView, int scrollState) {


                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
                    View centerView = snapHelper.findSnapView(mLayoutManager);
                    final int position = mLayoutManager.getPosition(centerView);

                    final ExoFullAdapter.CarsViewHolder holder = (ExoFullAdapter.CarsViewHolder) recyclerView.findViewHolderForAdapterPosition(position);


                    is1stEntered = false;

                    isPlaying = false;


                    if(!itemArrayList.get(position).getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                        System.out.println("only play >>>>>>>>> scroll   ");
                        holder.progressBar.setVisibility(View.VISIBLE);
                        holder.play.setVisibility(View.GONE);

                        posOfItem = position;

                        postViews(position);

                        float views = 0;
                        String vvv="";
                        views = itemArrayList.get(position).getViews();
                        views=views+1;
                        itemArrayList.get(position).setViews((int)views);
                        if(views<=999){
                            int vvvvv = (int)views;
                            vvv = String.valueOf(vvvvv)+" views";
                        }
                        else if(views >= 1000){
                            views = views/1000;
                            vvv = String.valueOf(views)+"k views";
                        }else if(views >= 1000000){
                            views = views/1000000;
                            vvv = String.valueOf(views)+"M views";
                        }


                        holder.tvViews.setText(vvv);



                        isReady = false;


                        playVideo(holder,position);



                    }else {
                        holder.progressBar.setVisibility(View.GONE);
                        holder.play.setVisibility(View.GONE);
                        if(exoPlayer != null){

                            exoPlayer.stop();
                        }
                    }

                }

                /*if (scrollState == RecyclerView.SCROLL_STATE_IDLE) {

                    layoutmanager = (LinearLayoutManager)recyclerView.getLayoutManager();

                    int firstVisiblePosition = layoutmanager.findFirstCompletelyVisibleItemPosition();

                    int ddd = layoutmanager.findFirstVisibleItemPosition();
                    int eee = layoutmanager.findLastCompletelyVisibleItemPosition();
                    int fff = layoutmanager.findLastVisibleItemPosition();

                    System.out.println("only ======================================= "+firstVisiblePosition);
                    System.out.println("only 1st full visible "+firstVisiblePosition);
                    System.out.println("only 1st partial visible "+ddd);
                    System.out.println("only last full visible "+eee);
                    System.out.println("only last partial visible "+fff);

                    if (firstVisiblePosition >= 0 || fff >= 0)
                    {
                        if (oldFocusedLayout != null)
                        {
                            System.out.println("only remove "+firstVisiblePosition);
                            if(exoPlayer != null){

                                carsViewHolder.play.setVisibility(View.VISIBLE);
                                carsViewHolder.imgCover.setVisibility(View.VISIBLE);
                                carsViewHolder.mExoPlayer.setVisibility(View.GONE);

                            }
                        }


                    }
                    if(exoPlayer != null){

                        exoPlayer.setPlayWhenReady(false);
                    }

                    currentFocusedLayout = layoutmanager.findViewByPosition(firstVisiblePosition);
                    //VideoView vv_dashboard = (VideoView)currentFocusedLayout.findViewById(R.id.vv_dashboard);
                    ////to play video of selected recylerview, videosData is an array-list which is send to recyclerview adapter to fill the view. Here we getting that specific video which is displayed through recyclerview.
                    //playVideo(videosData.get(positionView));

                    oldFocusedLayout = currentFocusedLayout;




                }*/
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });

        final Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(shouldPlay){
                    if(!itemArrayList.get(posOfItem).getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                        View centerView = snapHelper.findSnapView(mLayoutManager);
                        final int position = mLayoutManager.getPosition(centerView);

                        final ExoFullAdapter.CarsViewHolder holder = (ExoFullAdapter.CarsViewHolder) recyclerView.findViewHolderForAdapterPosition(position);

                        postViews(posOfItem);
                        holder.progressBar.setVisibility(View.VISIBLE);
                        holder.play.setVisibility(View.GONE);

                        float views = 0;
                        String vvv="";
                        views = itemArrayList.get(position).getViews();
                        views=views+1;
                        itemArrayList.get(position).setViews((int)views);
                        if(views<=999){
                            int vvvvv = (int)views;
                            vvv = String.valueOf(vvvvv)+" views";
                        }
                        else if(views >= 1000){
                            views = views/1000;
                            vvv = String.valueOf(views)+"k views";
                        }else if(views >= 1000000){
                            views = views/1000000;
                            vvv = String.valueOf(views)+"M views";
                        }

                        holder.tvViews.setText(vvv);


                        isReady = false;

                        playVideo(holder,position);

                    }else {

                        if(exoPlayer != null){
                            exoPlayer.stop();
                        }
                    }

                }
                else {
                    if (exoPlayer != null){
                        exoPlayer.stop();
                    }
                }


            }
        },500);




    }


    public void playVideo2(final ExoFullAdapter.CarsViewHolder holder,final int position){
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        if(exoPlayer != null){
            exoPlayer.setPlayWhenReady(false);
            exoPlayer.stop();
        }

        exoPlayer = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);


        CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

        MediaSource audioSource = new ExtractorMediaSource(Uri.parse(itemArrayList.get(position).getVideoUrl()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


        exoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                System.out.println("only= 1"+position);
            }
            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                System.out.println("only= 2 "+position);
            }
            @Override
            public void onLoadingChanged(boolean isLoading) {
                System.out.println("only= 3 "+position);
            }
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playWhenReady && playbackState == Player.STATE_READY) {
                    System.out.println("only= playing  "+position);
                    // media actually playing
                    holder.progressBar.setVisibility(View.GONE);
                    holder.play.setVisibility(View.GONE);
                    holder.imgCover.setVisibility(View.GONE);
                    holder.mExoPlayer.setVisibility(View.VISIBLE);

                    isPlaying = true;

                    isReady = true;
                } else if (playWhenReady) {
                    // might be idle (plays after prepare()),
                    // buffering (plays when data available)
                    // or ended (plays when seek away from end)
                    if (playbackState==Player.STATE_ENDED){
                        System.out.println("only=Ended  "+position);
                    }
                    else if (playbackState==Player.STATE_BUFFERING){
                        System.out.println("only=Buffering "+position);
                    }
                    else if (playbackState==Player.STATE_IDLE){
                        System.out.println("only=Idle "+position);

                    }
                } else {
                    // player paused in any state
                    System.out.println("only=paused  "+position);


                    //horizontalCarList.get(position).setPlayWhenReady(true);

                }
            }
            @Override
            public void onRepeatModeChanged(int repeatMode) {
                System.out.println("only= 4 "+position);
            }
            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
                System.out.println("only= 5 "+position);
            }
            @Override
            public void onPlayerError(ExoPlaybackException error) {
                System.out.println("only= 6  "+error.toString());
                //System.exit(0);
                //finish();



            }
            @Override
            public void onPositionDiscontinuity(int reason) {
                System.out.println("only= 7 "+position);
            }
            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                System.out.println("only= 8 "+position);
            }
            @Override
            public void onSeekProcessed() {

            }
        });


        exoPlayer.seekTo(0, 0);

        exoPlayer.prepare(audioSource, true, false);

        System.out.println("only=here  "+itemArrayList.get(position).getPrepared());

        exoPlayer.setPlayWhenReady(true);
        exoPlayer.getPlaybackState();


        holder.mExoPlayer.setPlayer(exoPlayer);





        adapterClickedPos=position;
        carsViewHolder = holder;
    }


    public void playVideo(final ExoFullAdapter.CarsViewHolder holder,final int position){

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        SimpleExoPlayer exoPlayer2 = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

        itemArrayList.get(position).setExoPlayer(exoPlayer2);
        itemArrayList2.get(position).setExoPlayer(exoPlayer2);


        if (exoPlayer != null){
            exoPlayer.stop();
        }

        exoPlayer = itemArrayList.get(position).getExoPlayer();

        CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

        MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(itemArrayList.get(position).getVideoUrl()));

        holder.mExoPlayer.setPlayer(exoPlayer);

        exoPlayer.seekTo(0, 0);

        exoPlayer.prepare(mediaSource, true, false);

        exoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                System.out.println("only= 1"+position);
            }
            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                System.out.println("only= 2 "+position);
            }
            @Override
            public void onLoadingChanged(boolean isLoading) {
                System.out.println("only= 3 "+position);
            }
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playWhenReady && playbackState == Player.STATE_READY) {
                    System.out.println("only= playing  "+position);
                    // media actually playing
                    holder.progressBar.setVisibility(View.GONE);
                    holder.play.setVisibility(View.GONE);
                    holder.imgCover.setVisibility(View.GONE);
                    holder.mExoPlayer.setVisibility(View.VISIBLE);

                    isPlaying = true;

                    isReady = true;
                } else if (playWhenReady) {
                    // might be idle (plays after prepare()),
                    // buffering (plays when data available)
                    // or ended (plays when seek away from end)
                    if (playbackState==Player.STATE_ENDED){
                        System.out.println("only=Ended  "+position);
                    }
                    else if (playbackState==Player.STATE_BUFFERING){
                        holder.progressBar.setVisibility(View.VISIBLE);
                        System.out.println("only=Buffering "+position);
                    }
                    else if (playbackState==Player.STATE_IDLE){
                        System.out.println("only=Idle "+position);

                    }
                } else {
                    // player paused in any state
                    System.out.println("only=   pause  "+position);


                    //horizontalCarList.get(position).setPlayWhenReady(true);

                }
            }
            @Override
            public void onRepeatModeChanged(int repeatMode) {
                System.out.println("only= 4 "+position);
            }
            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
                System.out.println("only= 5 "+position);
            }
            @Override
            public void onPlayerError(ExoPlaybackException error) {
                System.out.println("only= 6  "+error.toString());
                //System.exit(0);
                //finish();



            }
            @Override
            public void onPositionDiscontinuity(int reason) {
                System.out.println("only= 7 "+position);
            }
            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                System.out.println("only= 8 "+position);
            }
            @Override
            public void onSeekProcessed() {

            }
        });


        System.out.println("only=here  "+itemArrayList.get(position).getPrepared());

        exoPlayer.setPlayWhenReady(true);
        exoPlayer.getPlaybackState();




        itemArrayList.get(position).setExoPlayer(exoPlayer);
        itemArrayList2.get(position).setExoPlayer(exoPlayer);



        adapterClickedPos=position;
        carsViewHolder = holder;
    }



    public void postViews(final int position){
        StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/videoviews.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                    }
                }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("postid", itemArrayList2.get(position).getPostid());

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringrequest);
    }


    ExoFullAdapter.OnCarItemClickListener listener = new ExoFullAdapter.OnCarItemClickListener() {
        @Override
        public void onllClicked(final int position, PostItem item, final ExoFullAdapter.CarsViewHolder holder) {


            System.out.println("only  >>>>>>>>> "+holder.imgCover.getHeight()+" "+holder.imgCover.getWidth());
/*
            if(is1stEntered){
                is1stEntered = false;

                holder.progressBar.setVisibility(View.VISIBLE);
                holder.play.setVisibility(View.GONE);

                posOfItem = position;

                postViews(position);

                float views = 0;
                String vvv="";
                views = itemArrayList.get(position).getViews();
                views=views+1;
                itemArrayList.get(position).setViews((int)views);
                if(views<=999){
                    int vvvvv = (int)views;
                    vvv = String.valueOf(vvvvv)+" views";
                }
                else if(views >= 1000){
                    views = views/1000;
                    vvv = String.valueOf(views)+"k views";
                }else if(views >= 1000000){
                    views = views/1000000;
                    vvv = String.valueOf(views)+"M views";
                }


                holder.tvViews.setText(vvv);



                isReady = false;

                playVideo(holder,position);
            }
            else {
                if(exoPlayer != null){
                    if(isPlaying){
                        isPlaying = false;
                        exoPlayer.setPlayWhenReady(false);
                        exoPlayer.getPlaybackState();
                        holder.play.setVisibility(View.VISIBLE);
                    }else {
                        if(isReady){
                            exoPlayer.setPlayWhenReady(true);
                            exoPlayer.getPlaybackState();
                            isPlaying = true;
                            holder.play.setVisibility(View.GONE);
                        }
                    }


                }
            }*/

            if(exoPlayer != null){
                if(isPlaying){
                    isPlaying = false;
                    exoPlayer.setPlayWhenReady(false);
                    exoPlayer.getPlaybackState();
                    holder.play.setVisibility(View.VISIBLE);
                }else {
                    if(isReady){
                        exoPlayer.setPlayWhenReady(true);
                        exoPlayer.getPlaybackState();
                        isPlaying = true;
                        holder.play.setVisibility(View.GONE);
                    }
                }


            }

        }

        @Override
        public void onCoverClicked(int position, PostItem item, ExoFullAdapter.CarsViewHolder holder) {


        }

        @Override
        public void onWhatsAppClicked(final int position, PostItem item, ExoFullAdapter.CarsViewHolder viewHolder) {
            if (!itemArrayList2.get(position).getTime().contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(position).getText()+"\n"+whatsAppText);
                }
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Title", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }


                int shar = itemArrayList2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                itemArrayList2.get(position).setShares(shar);
                itemArrayList.get(position).setShares(shar);
                adapter.setItemlist(itemArrayList2);


                StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                            }
                        }

                        , new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("postid", itemArrayList2.get(position).getPostid());

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringrequest);

            }else {


                int shar = itemArrayList2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }


                downloadvideoWhatsApp downloadvideo2=new downloadvideoWhatsApp();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList2.get(position).getVideoUrl(),time2,String.valueOf(position));

            }
        }

        @Override
        public void onLikeClicked(final int position, PostItem item, ExoFullAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }else {
                viewHolder.imgLike.setSelected(!viewHolder.imgLike.isSelected());
                int count = db_sqliteFavorites.get_check_List_Favorite(itemArrayList2.get(position).getPostid());
                if (count == 0) {
                    viewHolder.imgLike.setImageResource(R.drawable.love2);
                    int lik = itemArrayList2.get(position).getLikes();

                    itemArrayList2.get(position).setLikes(lik+1);
                    itemArrayList.get(position).setLikes(lik+1);
                    adapter.setItemlist(itemArrayList2);
                    itemArrayList.get(position).setLiked(true);
                    itemArrayList2.get(position).setLiked(true);

                    adapter.setItemlist3(true,position);

                    db_sqliteFavorites.Inserttoalertlist(itemArrayList2.get(position).getPostid());

                    viewHolder.tvLikes.setText(String.valueOf(lik + 1));

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/sociallikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);
                } else {
                    viewHolder.imgLike.setImageResource(R.drawable.dislike);
                    db_sqliteFavorites.Delete(itemArrayList2.get(position).getPostid());

                    int lik = itemArrayList2.get(position).getLikes();

                    itemArrayList2.get(position).setLikes(lik-1);
                    itemArrayList.get(position).setLikes(lik-1);
                    adapter.setItemlist(itemArrayList2);
                    itemArrayList.get(position).setLiked(false);
                    itemArrayList2.get(position).setLiked(false);

                    adapter.setItemlist3(false,position);

                    if (lik == 0) {
                        viewHolder.tvLikes.setText("Like");
                    }

                    viewHolder.tvLikes.setText(String.valueOf(lik - 1));

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialdislikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
            }
        }

        @Override
        public void onCommentsClicked(int position, PostItem item, ExoFullAdapter.CarsViewHolder holder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                itemForComment = position;
                carsViewHolder_comments = holder;
                Intent intent=new Intent(getApplicationContext(),Comments.class);
                intent.putExtra("postid",itemArrayList2.get(position).getPostid());
                startActivityForResult(intent,11);
            }
        }

        @Override
        public void onDownloadsClicked(final int position, PostItem item, ExoFullAdapter.CarsViewHolder viewHolder) {
            viewHolder.progressBar_down.setVisibility(View.VISIBLE);

            if (!itemArrayList2.get(position).getTime().contains("/")){


                try{
                    Bitmap bm = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();

                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Flash/");

                    boolean doSave = true;
                    if (!f.exists()) {
                        doSave = f.mkdirs();
                    }

                    if (doSave) {
                        saveBitmapToFile(f,itemArrayList2.get(position).getPostid()+".png",bm,Bitmap.CompressFormat.PNG,100);
                    }
                    else {
                        Log.e("app","Couldn't create target directory.");
                    }


                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                    int down = itemArrayList2.get(position).getDownloadsno();
                    itemArrayList2.get(position).setDownloadsno(down+1);
                    itemArrayList.get(position).setDownloadsno(down+1);
                    viewHolder.tvDownloads.setText(String.valueOf(down+1));
                    adapter.setItemlist(itemArrayList2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {




                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("postid",itemArrayList2.get(position).postid);

                            return params;
                        }
                    };

                    RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                }catch (NullPointerException e){

                }




            }else {

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                int down = itemArrayList2.get(position).getDownloadsno();
                viewHolder.tvDownloads.setText(String.valueOf(down+1));

                downloadvideooo downloadvideo2=new downloadvideooo();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList2.get(position).getVideoUrl(),time2,String.valueOf(position));





            }





        }



        @Override
        public void onAddClicked(final int position, PostItem item, ExoFullAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                if (viewHolder.tvAdd.getText().toString().equals("Follow")) {
                    db_sqliteFollowing.Inserttoalertlist(itemArrayList2.get(position).getUid());
                    viewHolder.tvAdd.setText("Unfollow");

                    itemArrayList.get(position).setFollowing(true);
                    itemArrayList2.get(position).setFollowing(true);
                    adapter.setItemlist2(itemArrayList2.get(position).getUid(),true);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/followings.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList2.get(position).getUid());


                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }else {
                    db_sqliteFollowing.Delete(itemArrayList2.get(position).uid);
                    viewHolder.tvAdd.setText("Follow");

                    itemArrayList.get(position).setFollowing(false);
                    itemArrayList2.get(position).setFollowing(false);
                    adapter.setItemlist2(itemArrayList2.get(position).getUid(),false);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/unfollow.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList2.get(position).getUid());


                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
            }

        }

        @Override
        public void onProPicClicked(int position, PostItem item, ExoFullAdapter.CarsViewHolder holder) {
            if(useridd.equals(item.getUid())){
                Intent intent = new Intent(getApplicationContext(),Profile.class);
                startActivity(intent);

            }else {
                Intent intent = new Intent(getApplicationContext(),OtherUserProfile.class);
                intent.putExtra("uid",item.getUid());
                intent.putExtra("userName",item.getUsername());
                intent.putExtra("imgUrl",item.getPpurl());
                startActivity(intent);
            }

        }
    };






    class downloadvideooo extends AsyncTask<String,Integer,Void> {
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        String videoname;
        int pos;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Void doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                pos=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            circleProgressBar.setVisibility(View.INVISIBLE);
            linearLayout.setVisibility(View.VISIBLE);


            int down = itemArrayList2.get(pos).getDownloadsno();
            itemArrayList2.get(pos).setDownloadsno(down+1);
            itemArrayList.get(pos).setDownloadsno(down+1);
            adapter.setItemlist(itemArrayList2);

            StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {




                        }
                    }

                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();

                    params.put("postid",itemArrayList2.get(pos).postid);

                    return params;
                }
            };

            RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringrequest);


//            ddd ddd=new ddd();
//            ddd.setdownloadlayout(linearLayout);
//            ddd.setProgressbar(circleProgressBar);
//            ddd.execute(videoname);


        }
    }

    class downloadvideoWhatsApp extends AsyncTask<String,Integer,Boolean> {

        int positionn;
        String videoname;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
            progresss=0;
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                positionn=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){


                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);

                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList2.get(positionn).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(positionn).getText()+"\n"+whatsAppText);
                }
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                    int share = itemArrayList2.get(positionn).getShares();
                    itemArrayList2.get(positionn).setShares(share+1);
                    itemArrayList.get(positionn).setShares(share+1);
                    adapter.setItemlist(itemArrayList2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(positionn).getPostid());

                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }






                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.linearLayout.setVisibility(View.VISIBLE);

            }






        }


    }

    class downloadvideoshare extends AsyncTask<String,Integer,Boolean> {

        String videoname;
        int position;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                position=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");

                urlConnection.setRequestProperty("Accept-Encoding","identity");
                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;

                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);
                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){



                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);
                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                if (!itemArrayList2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(position).getText()+"\n"+whatsAppText);
                }
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);
            }






        }


    }




    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 11){

            int counter = data.getIntExtra("commentCounter",0);

            carsViewHolder_comments.tvComments.setText(String.valueOf(counter));
            System.out.println("only ff result back "+counter);
        }
    }
}
