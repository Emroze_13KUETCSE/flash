package com.status.fartech.socialmedia;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class WhatsAppAdapterSmall extends RecyclerView.Adapter<WhatsAppAdapterSmall.CarsViewHolder>{

    public ArrayList<WhatsAppItem> horizontalCarList;
    Context context;
    private OnCarItemClickListener listener;


    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onWhatsAppClicked(int position, WhatsAppItem item,CarsViewHolder holder);
    }
    public void setOnCarItemClickListener(OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public WhatsAppAdapterSmall(ArrayList<WhatsAppItem> horizontalGrocderyList, Context context){
        this.horizontalCarList= horizontalGrocderyList;
        this.context = context;


    }

    public void setItemlist(ArrayList<WhatsAppItem> list) {
        horizontalCarList = list;
    }


    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CarsViewHolder carsViewHolder, final int position) {


        carsViewHolder.proPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onWhatsAppClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });


        carsViewHolder.proPic.setImageBitmap(horizontalCarList.get(position).getBitmap());


    }






    @Override
    public void onViewRecycled(@NonNull CarsViewHolder holder) {
        int position = holder.getAdapterPosition();
        System.out.println("only recycled "+position);


        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        if (horizontalCarList != null) {
            return horizontalCarList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {

        CircleImageView proPic;


        public CarsViewHolder(View view) {
            super(view);
            proPic=view.findViewById(R.id.circle_friend2);

        }
    }
}
