package com.status.fartech.socialmedia;

import android.graphics.Bitmap;

public class WhatsAppItem {
    Bitmap bitmap;
    String filePath;
    String fileType;

    public WhatsAppItem() {
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }


}
